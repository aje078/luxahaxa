//
//  ColorDataModal.swift
//  LuxaHaxa
//
//  Created by Vijay on 14/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import Foundation

struct ColorDataModal {
 
    var color_id = 0
    var id = 0
    var color_name = ""
    var created_at = ""
    var status = ""
    var amount = ""
    var updated_at = ""
    
    init(dict: [String:Any], token:String = "") {
        
        self.color_id = dict["color_id"] as? Int ?? 0
        self.id = dict["id"] as? Int ?? 0
        self.color_name = dict["color_name"] as? String ?? ""
        self.created_at = dict["created_at"] as? String ?? ""
        self.status = dict["status"] as? String ?? ""
        self.amount = dict["amount"] as? String ?? ""
        self.updated_at = dict["updated_at"] as? String ?? ""
    }
}

struct futureGamesDataModal {
    
    var created_at = ""
    var game_id = ""
    var id = 0
    var status = ""
    var start_time = ""
    var end_time = ""
    var updated_at = ""
    
    init(dict: [String:Any], token:String = "") {
        
        self.game_id = dict["game_id"] as? String ?? ""
        self.id = dict["id"] as? Int ?? 0
        self.created_at = dict["created_at"] as? String ?? ""
        self.status = dict["status"] as? String ?? ""
        self.start_time = dict["start_time"] as? String ?? ""
        self.end_time = dict["end_time"] as? String ?? ""
        self.updated_at = dict["updated_at"] as? String ?? ""
    }
}

struct BidsResultDataModal {
  
    var amount = 0
    var color_id = 0
    var game_id = 0
    var id = 0
    var user_id = 0
    var winingamount = 0

    var created_at = ""
    var current_balance = ""
    var current_status = ""
    var status = ""
    var updated_at = ""
    
    init(dict: [String:Any], token:String = "") {
        
        self.amount = dict["amount"] as? Int ?? 0
        self.color_id = dict["color_id"] as? Int ?? 0
        self.game_id = dict["game_id"] as? Int ?? 0
        self.id = dict["id"] as? Int ?? 0
        self.user_id = dict["user_id"] as? Int ?? 0
        self.winingamount = dict["winingamount"] as? Int ?? 0

        
        self.created_at = dict["created_at"] as? String ?? ""
        self.status = dict["status"] as? String ?? ""
        self.current_balance = dict["current_balance"] as? String ?? ""
        self.current_status = dict["current_status"] as? String ?? ""
        self.updated_at = dict["updated_at"] as? String ?? ""
    }
}
