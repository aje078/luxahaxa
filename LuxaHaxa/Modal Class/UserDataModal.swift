//
//  UserDataModal.swift
//  Drive2uApp
//
//  Created by Vijay on 07/08/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//


import Foundation

struct UserDataModal {
    
    var updated_at = ""
    var name = ""
    var id = 0
    var token = ""
    var email = ""
    var created_at = ""
    var mobile = ""
    var otp = 0
    
    var user_balance = 0
    var month_buyed = 0
    var month_spent = 0
    
    var aadhar_file = ""
    var aadhar_number = ""
    var pan_file = ""
    var email_verified_at = ""
    var google_id = ""
    var bank_account_number = ""
    var bank_name = ""
    var facebook_id = ""
    var pan_number = ""
    var profile_file = ""
    var bank_branch_name = ""
    var phone = ""
    var bank_ifsc_code = ""
    
    init(dict: [String:Any], token:String = "", user_balance: Int = 0, month_buyed: Int = 0, month_spent: Int = 0) {
        
        self.user_balance = user_balance
        self.month_buyed = month_buyed
        self.month_spent = month_spent
        
        self.updated_at = dict["updated_at"] as? String ?? ""
        self.name = dict["name"] as? String ?? ""
        self.id = dict["id"] as? Int ?? 0
        self.token = token
        self.email = dict["email"] as? String ?? ""
        self.created_at = dict["created_at"] as? String ?? ""
        self.mobile = dict["mobile"] as? String ?? ""
        self.otp = dict["otp"] as? Int ?? 0
        
        self.aadhar_number = dict["aadhar_number"] as? String ?? ""
        self.email_verified_at = dict["email_verified_at"] as? String ?? ""
        self.google_id = dict["google_id"] as? String ?? ""
        self.bank_account_number = dict["bank_account_number"] as? String ?? ""
        self.bank_name = dict["bank_name"] as? String ?? ""
        self.facebook_id = dict["facebook_id"] as? String ?? ""
        self.pan_number = dict["pan_number"] as? String ?? ""
        self.bank_branch_name = dict["bank_branch_name"] as? String ?? ""
        self.phone = dict["phone"] as? String ?? ""
        self.bank_ifsc_code = dict["bank_ifsc_code"] as? String ?? ""
        
        let profilefile = dict["profile_file"] as? String ?? ""
        let panfile = dict["pan_file"] as? String ?? ""
        let aadharfile = dict["aadhar_file"] as? String ?? ""

        if profilefile.contains("public"){
            self.profile_file = "http://aryumoney.com/"+profilefile.replacingOccurrences(of: "public", with: "storage")
        }
        
        if panfile.contains("public"){
            self.pan_file = "http://aryumoney.com/"+panfile.replacingOccurrences(of: "public", with: "storage")
        }
        
        if aadharfile.contains("public"){
            self.aadhar_file = "http://aryumoney.com/"+aadharfile.replacingOccurrences(of: "public", with: "storage")
        }
        
        self.saveUserDefaultsData()
    }
    
    func saveUserDefaultsData(){
        
        UserDefaults.standard.setValue(self.user_balance, forKey: UserDefaults.Keys.user_balance)
        UserDefaults.standard.setValue(self.month_buyed, forKey: UserDefaults.Keys.month_buyed)
        UserDefaults.standard.setValue(self.month_spent, forKey: UserDefaults.Keys.month_spent)
        
        UserDefaults.standard.setValue(self.updated_at, forKey: UserDefaults.Keys.updated_at)
        UserDefaults.standard.setValue(self.name, forKey: UserDefaults.Keys.name)
        UserDefaults.standard.setValue(self.id, forKey: UserDefaults.Keys.id)
        UserDefaults.standard.setValue(self.token, forKey: UserDefaults.Keys.token)
        UserDefaults.standard.setValue(self.email, forKey: UserDefaults.Keys.email)
        UserDefaults.standard.setValue(self.created_at, forKey: UserDefaults.Keys.created_at)
        UserDefaults.standard.setValue(self.mobile, forKey: UserDefaults.Keys.mobile)
        
        UserDefaults.standard.setValue(self.aadhar_file, forKey: UserDefaults.Keys.aadhar_file)
        UserDefaults.standard.setValue(self.aadhar_number, forKey: UserDefaults.Keys.aadhar_number)
        UserDefaults.standard.setValue(self.pan_file, forKey: UserDefaults.Keys.pan_file)
        UserDefaults.standard.setValue(self.email_verified_at, forKey: UserDefaults.Keys.email_verified_at)
        UserDefaults.standard.setValue(self.google_id, forKey: UserDefaults.Keys.google_id)
        UserDefaults.standard.setValue(self.bank_account_number, forKey: UserDefaults.Keys.bank_account_number)
        UserDefaults.standard.setValue(self.bank_name, forKey: UserDefaults.Keys.bank_name)
        UserDefaults.standard.setValue(self.facebook_id, forKey: UserDefaults.Keys.facebook_id)
        UserDefaults.standard.setValue(self.pan_number, forKey: UserDefaults.Keys.pan_number)
        UserDefaults.standard.setValue(self.profile_file, forKey: UserDefaults.Keys.profile_file)
        UserDefaults.standard.setValue(self.bank_branch_name, forKey: UserDefaults.Keys.bank_branch_name)
        UserDefaults.standard.setValue(self.phone, forKey: UserDefaults.Keys.phone)
        UserDefaults.standard.setValue(self.bank_ifsc_code, forKey: UserDefaults.Keys.bank_ifsc_code)
    }
}
