//
//  WalletCell.swift
//  LuxaHaxa
//
//  Created by Vijay on 04/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class WalletCell: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!{
        didSet{
            viewCell.setViewShadowWithCorner(radius: 5, opacity: 0.8)
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
