//
//  Terms_ConditionVC.swift
//  Appointment
//
//  Created by Apple on 21/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WebKit

class Terms_ConditionVC: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var txtViewAboutUS: UITextView!
    var strAboutUS:String = ""
    var isFromSocial:Bool = false
    var isFacebookLogin:Bool = false
    var dictSocial:[String:Any] = [:]
    
    @IBOutlet weak var lblHeading: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.call_for_webservice_GetContent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnAccept(_ sender: Any) {
        self.view.endEditing(true)
        
        if self.isFromSocial == true{
            self.callWebServie_Customer_SocialLogin()
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func btnDecline(_ sender: Any) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    func setHtMLDatToTextView(str:String){
    
        self.txtViewAboutUS.attributedText = "\(str)".html2AttributedString
        self.txtViewAboutUS.font = self.txtViewAboutUS.font?.withSize(18)
    }
    
    // MARK: - Webservice Methods
    func callWebServie_Customer_Login(){
        
         let name = self.dictSocial["name"] as? String ?? ""
         let email = self.dictSocial["email"] as? String ?? ""
         let mobile = self.dictSocial["mobile"] as? String ?? ""
         let password = self.dictSocial["password"] as? String ?? ""
         let confirmPassword = self.dictSocial["password_confirmation"] as? String ?? ""
               
        let dict = ["email": name, "password": password]
        
        WebServiceManager.sharedManager.requestPost(strURL: kUrlSignIn, params: dict, success: { (response) in
            print(response)
            
            let error = response["message"] as? String
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let token = data["token"] as? String ?? ""
            if code == 200 || status == "success"{
                
                let _ = UserDataModal.init(dict: user, token: token)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                ValidationManager.sharedManager.showAlert(message: error ?? "Something went wrong, please try agian", title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try again", title: "Alert", controller: self)
            
        }
    }
    
    func callWebServie_Customer_SocialLogin(){
        
        let name = self.dictSocial["name"] as? String ?? ""
        let email = self.dictSocial["email"] as? String ?? ""
        let mobile = self.dictSocial["mobile"] as? String ?? ""
        let password = self.dictSocial["password"] as? String ?? ""
        let confirmPassword = self.dictSocial["password_confirmation"] as? String ?? ""
        let socialLoginId = self.dictSocial["socialLoginId"] as? String ?? ""
        
        var urlType = kUrlGoogleSignIn
        var socialKey = "google_id"
        if self.isFacebookLogin == true{
            urlType = kUrlFacebookSignIn
            socialKey = "facebook_id"
        }
       
        let dict = ["name": name, "email": email, socialKey: socialLoginId]
        
        WebServiceManager.sharedManager.requestPost(strURL: urlType, params: dict, success: { (response) in
            print(response)
            
            let error = response["error"] as? [String]
            var errMsg = "Something went wrong, please try agian"
            if error?.count ?? 0 > 0{
                errMsg = error?[0] ?? "Something went wrong, please try agian"
            }
            
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let token = data["token"] as? String ?? ""
            if code == 200 || status == "success"{
                
                let _ = UserDataModal.init(dict: user, token: token)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                ValidationManager.sharedManager.showAlert(message: errMsg, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try agian", title: "Alert", controller: self)
            
        }
    }
    
    //GetContent API Calling
    func call_for_webservice_GetContent() -> Void {
        
        let url = BaseURL+kUrlTermsAndCondition
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { responseObject in
                        
            switch responseObject.result {
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                
                let str = String(decoding:  responseObject.data!, as: UTF8.self)
                self.setHtMLDatToTextView(str: str)
                
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    //success(dictionary as! Dictionary<String, Any>)
                    
                    
                    print(dictionary)
                }catch{
                    if let error : Error = responseObject.error{
                        //failure(error)
                        let str = String(decoding:  responseObject.data!, as: UTF8.self)
                        print("PHP ERROR : \(str)")
                        self.setHtMLDatToTextView(str: str)
                    }

                }

            case .failure(let error):
                
                print(error)
                if let error : Error = responseObject.error{
                    if let error = responseObject.data{
                        let str = String(decoding:  error, as: UTF8.self)
                        print("PHP ERROR : \(str)")

                        self.setHtMLDatToTextView(str: str)
                    }
                }
            }
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView){
        WebServiceManager.sharedManager.startAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        WebServiceManager.sharedManager.stopAnimating()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        WebServiceManager.sharedManager.stopAnimating()
    }

    
}


extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

extension String {
    
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSMutableAttributedString(data: data,
                                                 options: [.documentType: NSMutableAttributedString.DocumentType.html,
                                                           .characterEncoding: String.Encoding.utf8.rawValue],
                                                 documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
}
