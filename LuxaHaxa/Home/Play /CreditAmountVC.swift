//
//  CreditAmountVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 10/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit
import CryptoSwift

class CreditAmountVC: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtRemark: UITextField!
    
    @IBOutlet weak var viewInAmount: UIView!
    @IBOutlet weak var viewInRemark: UIView!
    @IBOutlet weak var hViewInAmount: NSLayoutConstraint!
    @IBOutlet weak var hViewInRemark: NSLayoutConstraint!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRemark: UILabel!
    
    @IBOutlet weak var bLblAmount: NSLayoutConstraint!
    @IBOutlet weak var bLblRemark: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func configureViewController(){
        self.height_required()

        self.configureUserData()
    }
    
    func configureUserData(){

        txtAmount.delegate = self
        txtRemark.delegate = self
       
        self.hViewInAmount.constant = 1
        self.hViewInRemark.constant = 1
        
    }
    
    func setupPayu(){
                
        let kMerchantSalt = KMerchantSaltKey
        
        var txnParam = PUMTxnParam()
        
        PlugNPlay.setDisableCompletionScreen(true)
        
        //Set the parameters
        
        txnParam.phone = UserDefaults.standard.string(forKey: UserDefaults.Keys.mobile)
        if txnParam.phone == ""{
            txnParam.phone = "1234567890"
        }
        
        txnParam.email = UserDefaults.standard.string(forKey: UserDefaults.Keys.email)
        
        txnParam.amount = self.txtAmount.text!
        
        txnParam.environment = PUMEnvironment.test
        
        txnParam.firstname = UserDefaults.standard.string(forKey: UserDefaults.Keys.name)
        
        txnParam.key = KMerchantKey
        
        txnParam.merchantid = KMerchantId
        
        txnParam.txnID = "1234567890"
        
        txnParam.surl = "https://www.payumoney.com/mobileapp/payumoney/success.php"
        
        txnParam.furl = "https://www.payumoney.com/mobileapp/payumoney/failure.php"
        
        txnParam.productInfo = UIDevice.modelName
        
        txnParam.udf1 = "ud1"
        
        txnParam.udf2 = "ud2"
        
        txnParam.udf3 = "ud3"
        
        txnParam.udf4 = "ud4"
        
        txnParam.udf5 = "ud5"
        
        txnParam.udf6 = ""
        
        txnParam.udf7 = ""
        
        txnParam.udf8 = ""
        
        txnParam.udf9 = ""
        
        txnParam.udf10 = ""
        
        /*
         
         * Below code is used to create a formatted hashString from the Payu Transaction parameters
         
         * **Author:** Asharma
         
         * **Date:** 09-Oct-2018
         
         */
        
        let hashString = "\(txnParam.key!)|\(txnParam.txnID!)|\(txnParam.amount!)|\(txnParam.productInfo!)|\(txnParam.firstname!)|\(txnParam.email!)|\(txnParam.udf1!)|\(txnParam.udf2!)|\(txnParam.udf3!)|\(txnParam.udf4!)|\(txnParam.udf5!)||||||\(kMerchantSalt)"
        
        /*
         
         * Below code is used to create dynamic hash from hashString
         
         * **Author:** Asharma
         
         * **Date:** 09-Oct-2018
         
         */
        
        let data = hashString.data(using: .utf8)
        //4012001037141112
        txnParam.hashValue = data?.sha512().toHexString()
        PlugNPlay.presentPaymentViewController(withTxnParams: txnParam, on: self) { (response, error, extraParam) in
            
            print(error)

            if error != nil{
                var errMsg = error?.localizedDescription
                if error?.localizedDescription.contains("mobile") ?? true{
                    errMsg = "Please update your mobile number from profile"
                }else if error?.localizedDescription.contains("email") ?? true{
                    errMsg = "Please update your email id from profile"
                }
                ValidationManager.sharedManager.showAlert(message: errMsg ?? "Something went wrong", controller: self)
            }else{
                self.callWebServie_SubmitDetails()
            }
        }
    }
    
        
        @IBAction func btnSubmitDetails(_ sender: Any) {
            
            if (txtAmount.text?.isEmpty)!
            {
                ValidationManager.sharedManager.showAlert(message: "Please enter amount", title: "Alert", controller: self)
            }else if (txtRemark.text?.isEmpty)!
            {
                ValidationManager.sharedManager.showAlert(message: "Please enter remark", title: "Alert", controller: self)
            }else{
                
                self.setupPayu()
                
            }
        }
        
        
        // MARK: - Text Field Methods
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == txtAmount{
                txtRemark.becomeFirstResponder()
            }else if textField == txtRemark{
                txtRemark.resignFirstResponder()
            }
            
            return true
        }
        
        
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            
            if textField == txtAmount{

                self.height_required()
                self.hViewInAmount.constant = 2
                
                self.bLblAmount.constant = 20
                self.lblAmount.font = lblAmount.font.withSize(14)
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }

            }else if textField == txtRemark{
                self.height_required()
                self.hViewInRemark.constant = 2

                self.bLblRemark.constant = 20
                self.lblRemark.font = lblRemark.font.withSize(14)
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }

            return true

        }
        
        func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            
            if textField == txtAmount{
                if self.txtAmount.text?.count == 0{
                    self.bLblAmount.constant = 3
                    self.lblAmount.font = lblAmount.font.withSize(17)
                }
            }else if textField == txtRemark{
                if self.txtRemark.text?.count == 0{
                    self.bLblRemark.constant = 3
                    self.lblRemark.font = lblRemark.font.withSize(17)
                }
            }
            
            return true
        }
        
        func height_required(){
            
            self.hViewInAmount.constant = 1
            self.hViewInRemark.constant = 1
            
            self.lblAmount.text! = "AMOUNT"
            self.lblRemark.text! = "REMARK"
           
            if self.txtAmount.text?.count == 0{
                self.bLblAmount.constant = 3
                self.lblAmount.font = lblAmount.font.withSize(17)
            }
            
            if self.txtRemark.text?.count == 0{
                self.bLblRemark.constant = 3
                self.lblRemark.font = lblRemark.font.withSize(17)
            }
            

        }        
        
    }

    // MARK: - Webservice Methods

    extension CreditAmountVC{
                
        func callWebServie_SubmitDetails(){
           
            let dict = ["transaction_id": "11561212315", "payment_mode": "Paytm", "amount": self.txtAmount.text!, "status": self.txtRemark.text!]
            
            WebServiceManager.sharedManager.requestPost(strURL: kUrlBuy_money, params: dict, success: { (response) in
                print(response)
                
                let error = response["error"] as? [String]
                var errMsg = "Something went wrong, please try agian"
                if error?.count ?? 0 > 0{
                    errMsg = error?[0] as? String ?? "Something went wrong, please try agian"
                }
                
                let code = response["code"] as? Int
                let status = response["status"] as? String ?? ""
                let data = response["data"] as? [String:Any] ?? [:]
                let user = data["record"] as? [String:Any] ?? [:]
                let token = data["token"] as? String ?? ""
                if code == 200 || status == "success"{
                    
                    print("details Updated")

                    ValidationManager.sharedManager.showAlertWithAction(title: "Alert", message: "Amount credited to your account", controller: self) { (status) in
                        
                        self.navigationController?.popViewController(animated: true)
                    }

                }else{
                    ValidationManager.sharedManager.showAlert(message: errMsg, title: "Alert", controller: self)
                }
            }) { (error) in
                print(error)
                ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try agian", title: "Alert", controller: self)
                
            }
        }
        
    }




