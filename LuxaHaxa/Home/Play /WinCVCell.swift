//
//  WinCVCell.swift
//  LuxaHaxa
//
//  Created by Vijay on 16/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class WinCVCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imgDart: UIImageView!{
        didSet{
            imgDart.setCornerRadius(cornerRadius: 3, borderColor: nil, borderWidth: nil)
        }
    }
    @IBOutlet weak var btnCell: UIButton!
    
}
