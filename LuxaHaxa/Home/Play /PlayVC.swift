//
//  PlayVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 08/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//


import UIKit

class Cell_ForColors: UICollectionViewCell {
    
    @IBOutlet weak var viewMain: UIView!{
        didSet{
            viewMain.setCornerRadius(cornerRadius: 10, borderColor: nil, borderWidth: nil)
        }
    }
    
    @IBOutlet weak var viewBorder: UIView!
}

class PlayVC: BaseViewController, SWRevealViewControllerDelegate, UIGestureRecognizerDelegate{
    
    var arrColors  = [ColorDataModal]()
    var arrSelectedColors  = [Int]()
    var arrFutureGames  = [futureGamesDataModal]()
    var arrBidResultGames  = [BidsResultDataModal]()

    var lastgameStartTime = ""
    var lastgameEndTime = ""
    
    var arrWinningColors = [String]()
    
    let arrWin1 = [#colorLiteral(red: 0.003921568627, green: 0.937254902, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.9803921569, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.1647058824, green: 0.862745098, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.7607843137, green: 0.1490196078, blue: 0.7725490196, alpha: 1), #colorLiteral(red: 0.9176470588, green: 0.8588235294, blue: 0.06274509804, alpha: 1), #colorLiteral(red: 0.1725490196, green: 0.9254901961, blue: 0.07450980392, alpha: 1), #colorLiteral(red: 0.9803921569, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.1647058824, green: 0.862745098, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.7607843137, green: 0.1490196078, blue: 0.7725490196, alpha: 1), #colorLiteral(red: 0.9176470588, green: 0.8588235294, blue: 0.06274509804, alpha: 1), #colorLiteral(red: 0.1725490196, green: 0.9254901961, blue: 0.07450980392, alpha: 1), #colorLiteral(red: 0.003921568627, green: 0.937254902, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.1647058824, green: 0.862745098, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.7607843137, green: 0.1490196078, blue: 0.7725490196, alpha: 1), #colorLiteral(red: 0.9176470588, green: 0.8588235294, blue: 0.06274509804, alpha: 1), #colorLiteral(red: 0.1725490196, green: 0.9254901961, blue: 0.07450980392, alpha: 1), #colorLiteral(red: 0.003921568627, green: 0.937254902, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.9803921569, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.7607843137, green: 0.1490196078, blue: 0.7725490196, alpha: 1), #colorLiteral(red: 0.9176470588, green: 0.8588235294, blue: 0.06274509804, alpha: 1), #colorLiteral(red: 0.1725490196, green: 0.9254901961, blue: 0.07450980392, alpha: 1), #colorLiteral(red: 0.003921568627, green: 0.937254902, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.9803921569, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.1647058824, green: 0.862745098, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.9176470588, green: 0.8588235294, blue: 0.06274509804, alpha: 1), #colorLiteral(red: 0.1725490196, green: 0.9254901961, blue: 0.07450980392, alpha: 1), #colorLiteral(red: 0.003921568627, green: 0.937254902, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.9803921569, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.1647058824, green: 0.862745098, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.7607843137, green: 0.1490196078, blue: 0.7725490196, alpha: 1), #colorLiteral(red: 0.1725490196, green: 0.9254901961, blue: 0.07450980392, alpha: 1), #colorLiteral(red: 0.003921568627, green: 0.937254902, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.9803921569, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.1647058824, green: 0.862745098, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.7607843137, green: 0.1490196078, blue: 0.7725490196, alpha: 1), #colorLiteral(red: 0.9176470588, green: 0.8588235294, blue: 0.06274509804, alpha: 1)]
    
    //    var arrWin = [UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5")]
    
    var arrWin = [UIColor(hexString: "#01ef89"), UIColor(hexString: "#fa0001"), UIColor(hexString: "#2adcd9"), UIColor(hexString: "#c226c7"), UIColor(hexString: "#eadb16"), UIColor(hexString: "#2cec14"), UIColor(hexString: "#2adcd5"), UIColor(hexString: "#c226c3"), UIColor(hexString: "#eadb12"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#01ef80"), UIColor(hexString: "#fa0002"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#01ef81"), UIColor(hexString: "#fa0003"), UIColor(hexString: "#2adcd4"), UIColor(hexString: "#c226c8"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5"), UIColor(hexString: "#eadb14"), UIColor(hexString: "#2cec16"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec15"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#eadb10"), UIColor(hexString: "#2cec13"), UIColor(hexString: "#01ef88"), UIColor(hexString: "#fa0000"), UIColor(hexString: "#2adcdf"), UIColor(hexString: "#c226c5")]
    
    var arrWinString: [String] = []
    
    var swipeGesture = UISwipeGestureRecognizer()
    
    var strSelectedBid = ""
    var strSelectedGameId = ""
    var strSelectedColorId = ""
    var selectedIndex:Int = 10000
    
    var arrBidPrice = ["₹ 50", "₹ 100", "₹ 200", "₹ 300", "₹ 400", "₹ 500", "₹ 1000", "₹ 1500", "₹ 2000", "₹ 2500", "₹ 3000", "₹ 3500", "₹ 4000", "₹ 4500", "₹ 5000"]
    
    var timer: Timer?
    var blockTimer: Timer?
    var totalTime = 300
    var blockTime = 10
    var strSelectedColorIndex = 10000000
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var collectionViewColors: UICollectionView!
    
    @IBOutlet weak var viewBlock: UIView!
    @IBOutlet weak var viewSummery: UIView!
    
    @IBOutlet weak var viewSelectedBid: UIView!
    @IBOutlet weak var lblSelectedBid: UILabel!
    @IBOutlet weak var viewMinMax: UIView!
    @IBOutlet weak var lblMinBid: UILabel!
    @IBOutlet weak var lblMaxBid: UILabel!
    @IBOutlet weak var viewMin: UIView!
    @IBOutlet weak var viewMax: UIView!
    @IBOutlet weak var viewBiddingDone: UIView!
    @IBOutlet weak var viewBiddingDoneIn: UIView!
    @IBOutlet weak var btnBiddingOk: UIButton!
    @IBOutlet weak var viewImgCorrect: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewWin: UIView!
    @IBOutlet weak var viewWinIn: UIView!
    
    @IBOutlet weak var lblBlockTime: UILabel!
    @IBOutlet weak var lblBlockTitle: UILabel!
    
    @IBOutlet weak var lblSummeryTitle: UILabel!
    @IBOutlet weak var lblResultType: UILabel!
    
    @IBOutlet weak var collectionViewBid: UICollectionView!
    @IBOutlet weak var collectionViewWin: UICollectionView!
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewBidMain: UIView!
    @IBOutlet weak var viewBid: UIView!
    
    @IBOutlet weak var bottomViewBid: NSLayoutConstraint!
    
    /// The currently selected curve to be used for all animations
    var selectedCurve: UIView.AnimationCurve = .easeInOut
    
    /// A list of colors to be used when animating the background color
    let colors: [UIColor] = [.white, .red, .green, .orange, .yellow, .lightGray, .darkGray]
    
    /// The ⬇️ button that rotates and moves around the screen
    @IBOutlet var movingArrowView1: UIView!
    @IBOutlet var movingArrowView2: UIView!
    @IBOutlet var movingArrowView3: UIView!
    @IBOutlet var movingArrowImageView1: UIImageView!
    @IBOutlet var movingArrowImageView2: UIImageView!
    @IBOutlet var movingArrowImageView3: UIImageView!
    @IBOutlet var ArcherImage: UIImageView!
    
    var isShowBlockView = false
    var isShowWinView = false
    var isCallTimerApi = false
    var isShowSummeryView = false
    var isShowNewGameView = false
    var isBiddingDone = false
    var isNotEligibleForGame = false
    
    /// The fake HUD view
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.movingArrowImageView1.image = nil
        self.movingArrowImageView2.image = nil
        self.movingArrowImageView3.image = nil
        
        self.arrSelectedColors = UserDefaults.standard.array(forKey: UserDefaults.Keys.arrSelectedColors) as? [Int] ?? []
                
        self.configureViewController()
        
        menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.revealViewController().delegate=self
        
        self.arrWin = self.arrWin.shuffled()
        
        self.swipeUptoBid()
        self.callWebServie_Customer_GetDashboard()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    
    @IBAction func moveTo(button: UIButton) {
        let destination = view.convert(button.center, from: button.superview)
        //       movingArrowView.move(to: destination.applying(CGAffineTransform(translationX: 0.0, y: -10.0)),
        //                         duration: 1.0,
        //                         options: selectedCurve.animationOption)
    }
    
//    @IBAction func rotate(button: UIButton) {
//        button.rotate180(duration: 1.0, options: selectedCurve.animationOption)
//    }
    
//    @IBAction func zoomIn(button: UIButton) {
//        let pickerVC = AnimationCurvePickerViewController(style: .grouped)
//        pickerVC.delegate = self
//        pickerVC.view.bounds = CGRect(x: 0, y: 0, width: 280, height: 300)
//        pickerVC.view.center = view.center
//
//        addChild(pickerVC)
//        view.addSubviewWithZoomInAnimation(pickerVC.view, duration: 1.0, options: selectedCurve.animationOption)
//        pickerVC.didMove(toParent: self)
//    }
//
//    @IBAction func changeBackgroundColor() {
//        view.changeColor(to: colors.randomElement()!, duration: 1.0, options: selectedCurve.animationOption)
//    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    func utcToLocal(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "h:mm a"
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    @objc func updateBlockTimer() {
       
        if blockTime != 0 {
            blockTime -= 1  // decrease counter timer
        }
        
//        if self.isBiddingDone == true{
//            self.lblBlockTime.text = self.lblTime.text
//        }else{
            self.lblBlockTime.text = self.timeFormatted(self.blockTime) // will show timer
            if blockTime == 0 {
                                
                self.blockTimer?.invalidate() // decrease counter timer
                if self.arrFutureGames.count == 0 || self.isNotEligibleForGame == true{
                    self.isNotEligibleForGame = false
                    AppDelegate.sharedManager.navigateToStartGame()
                }
            }
//        }
    }
    @objc func updateTimer() {

        if totalTime != 0 {
            
            if totalTime <= 80, self.isShowBlockView == false{
                
                self.isShowBlockView = true
                self.blockTimer?.invalidate()
                self.blockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateBlockTimer), userInfo: nil, repeats: true)
                                
                self.viewBidMain.isHidden = true
                self.bottomViewBid.constant = -700
                self.lblBlockTitle.text = "Result in"
                self.viewBlock.isHidden = false
                return
                
            }
            else if totalTime <= 70, self.isShowWinView == false{
                
                self.isShowWinView = true
                self.lblTime.isHidden = false
                self.viewBlock.isHidden = true
                self.viewWin.isHidden = false  // decrease counter timer
                return
            }
            else if totalTime <= 65, self.isCallTimerApi == false{
                
                self.isCallTimerApi = true
                self.callWebServie_Customer_GetDashboard(isFromTimer: true)
                return
            }
            else if totalTime <= 50, self.isShowSummeryView == false{
                
                if self.strSelectedGameId == ""{
                    self.strSelectedGameId = arrFutureGames[0].game_id
                }
                self.callWebServie_GetResult()
//                self.isShowSummeryView = true
//                self.viewSummery.isHidden = false  // decrease counter timer
                return
            }
            else if totalTime <= 35, self.isShowNewGameView == false{
                                    
                self.isShowNewGameView = true
                self.viewSummery.isHidden = true
                                
                self.lblBlockTitle.text = "New game in"
                UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.arrSelectedColors)
                self.viewBlock.isHidden = false
                return
            }
            else if totalTime <= 9, self.arrFutureGames.count > 0{
                
                self.timer?.invalidate()
                self.blockTimer?.invalidate()
                self.viewBlock.isHidden = true
                
                UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.arrSelectedColors)
                AppDelegate.sharedManager.navigateToStartGame()
            }
            totalTime -= 1  // decrease counter timer
        }
        
        self.lblTime.text = self.timeFormatted(self.totalTime) // will show timer
        if self.isShowNewGameView == true{
            self.lblTime.isHidden = true
            var time = self.lblTime.text!
            time = time.replacingOccurrences(of: "00:", with: "")
            let timeInt = Int(time) ?? 10
            let finalTime = timeInt - 9
            self.lblBlockTime.text = "\(finalTime)"
        }
        
    }
    
    func checkWhichGameIsInCurrentTimeSlot(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strCurrentTime = formatter.string(from: Date())
        
        if self.arrFutureGames.count == 0{
                        
            if let gameEndTime = formatter.date(from: self.lastgameEndTime), let currentTime = formatter.date(from: strCurrentTime){
                
                print(gameEndTime)
                print(currentTime)

                if currentTime > gameEndTime{
                    
                    self.lblBlockTitle.text = "Your game will start"
                    self.lblBlockTime.text = "Soon"
                    self.viewBlock.isHidden = false
                    
                    let totalSeconds = currentTime.seconds(from: gameEndTime)
                    var countOfSeconds = 300 - totalSeconds
                    
                    if countOfSeconds < 0{
                        countOfSeconds = -(countOfSeconds)
                    }
                    
                    self.blockTime = countOfSeconds
                    self.blockTimer?.invalidate()
                    self.blockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateBlockTimer), userInfo: nil, repeats: true)
                    
                }
            }
                        
            return
        }
                
//        for obj in self.arrFutureGames{
        let obj = self.arrFutureGames[0]
            
            if let gameStartTime = formatter.date(from: obj.start_time), let currentTime = formatter.date(from: strCurrentTime){
                
                print(gameStartTime)
                print(currentTime)

                if currentTime > gameStartTime{
                    
                    let totalSeconds = currentTime.seconds(from: gameStartTime)
                    var countOfSeconds = 300 - totalSeconds
                    
                    if countOfSeconds < 0{
                        countOfSeconds = -(countOfSeconds)
                    }
                    
                    self.totalTime = countOfSeconds
                    self.timer?.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
                    
//                    break
                }else if let gameEndTime = formatter.date(from: self.lastgameEndTime), let gameStartTime = formatter.date(from: self.lastgameStartTime), currentTime > gameEndTime{
                    
                    print(gameStartTime)
                    print(gameEndTime)
                    print(currentTime)
                    
                    if currentTime > gameEndTime{
                        
                        self.lblBlockTitle.text = "New game will start in"
                        self.lblBlockTime.text = "Soon"
                        self.viewBlock.isHidden = false

                        let totalSeconds = currentTime.seconds(from: gameStartTime)
                        var countOfSeconds = 300 - totalSeconds

                        if countOfSeconds < 0{
                            countOfSeconds = -(countOfSeconds)
                        }

                        self.isNotEligibleForGame = true
                        self.blockTime = countOfSeconds
                        self.blockTimer?.invalidate()
                        self.blockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateBlockTimer), userInfo: nil, repeats: true)
                    }
                }
            }
//        }
    }
    
    func configureViewController(){
        
        self.viewSelectedBid.setCornerRadius(cornerRadius: 3, borderColor: UIColor.lightGray, borderWidth: 0.5)
        self.viewMin.setCornerRadius(cornerRadius: 3, borderColor: UIColor.lightGray, borderWidth: 0.5)
        self.viewMax.setCornerRadius(cornerRadius: 3, borderColor: UIColor.lightGray, borderWidth: 0.5)
        self.viewBiddingDoneIn.setCornerRadius(cornerRadius: 20, borderColor: nil, borderWidth: nil)
        self.btnBiddingOk.setCornerRadius(cornerRadius: 5, borderColor: nil, borderWidth: nil)
        self.viewImgCorrect.setCornerRadius(cornerRadius: nil, borderColor: UIColor.lightGray, borderWidth: 1)
        self.viewWinIn.setCornerRadius(cornerRadius: 10, borderColor: UIColor.lightGray, borderWidth: 0.5)
        
        self.viewImgCorrect.layer.cornerRadius = viewImgCorrect.frame.size.height/2
        
        self.viewBidMain.isHidden = true
        self.bottomViewBid.constant = -700
        self.viewBiddingDone.isHidden = true
        
        self.isShowBlockView = false
        self.isShowWinView = false
        self.isCallTimerApi = false
        self.isShowSummeryView = false
        self.isShowNewGameView = false
        
        self.viewWin.isHidden = true
        self.viewBlock.isHidden = true
        self.viewSummery.isHidden = true
        self.ArcherImage.isHidden = false
        
        //self.ArcherImage.image = UIImage.gifImageWithName("Archer")
        
        
    }
    
    func setMovingArrowsToTarget(){
        
        var yPosition:CGFloat = -120
        if #available(iOS 11.0, *), (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)! > CGFloat(0){
            yPosition = -140
        }
        
        for index in 0..<arrWinningColors.count{
            
            let color = arrWinningColors[index]
            let colorIndex = self.arrWin.firstIndex(of: UIColor(hexString: color))
            
            self.ArcherImage.isHidden = false
            if index == 0 {
                
                let indexPath = IndexPath.init(item: colorIndex ?? 0, section: 0)
                let cell = self.collectionViewWin.cellForItem(at: indexPath) as! WinCVCell
                let button = cell.btnCell
                
                let destination = self.view.convert(button?.center ?? CGPoint(x: 0, y: 0), from: button?.superview)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                    let jeremyGif = UIImage.gifImageWithName("Archer2")
                    self.ArcherImage.animationImages = jeremyGif?.images
                    self.ArcherImage.animationDuration = jeremyGif!.duration
                    self.ArcherImage.animationRepeatCount = 1
                    self.ArcherImage.startAnimating()
                    self.movingArrowImageView1.image = #imageLiteral(resourceName: "Arrow")
                    self.movingArrowView1.move(to: destination.applying(CGAffineTransform(translationX: 0.0, y: yPosition)), duration: 1.0, options: self.selectedCurve.animationOption)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                    self.movingArrowImageView1.image = nil
                    cell.imgDart.image = #imageLiteral(resourceName: "Arrow")//UIImage.gifImageWithName("dartTargetGIF")
                }
            }
            else if index == 1 {
                
                let indexPath = IndexPath.init(item: colorIndex ?? 0, section: 0)
                let cell = self.collectionViewWin.cellForItem(at: indexPath) as! WinCVCell
                let button = cell.btnCell
                
                let destination = self.view.convert(button?.center ?? CGPoint(x: 0, y: 0), from: button?.superview)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+5) {
                    let jeremyGif = UIImage.gifImageWithName("Archer2")
                    self.ArcherImage.animationImages = jeremyGif?.images
                    self.ArcherImage.animationDuration = jeremyGif!.duration
                    self.ArcherImage.animationRepeatCount = 1
                    self.ArcherImage.startAnimating()
                    self.movingArrowImageView2.image = #imageLiteral(resourceName: "Arrow")
                    self.movingArrowView2.move(to: destination.applying(CGAffineTransform(translationX: 0.0, y: yPosition)), duration: 1.0, options: self.selectedCurve.animationOption)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+6) {
                    self.movingArrowImageView2.image = nil
                    cell.imgDart.image = #imageLiteral(resourceName: "Arrow")//UIImage.gifImageWithName("dartTargetGIF")
                }
            }else if index == 2 {
                
                let indexPath = IndexPath.init(item: colorIndex ?? 0, section: 0)
                let cell = self.collectionViewWin.cellForItem(at: indexPath) as! WinCVCell
                let button = cell.btnCell
                
                let destination = self.view.convert(button?.center ?? CGPoint(x: 0, y: 0), from: button?.superview)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+8) {
                    let jeremyGif = UIImage.gifImageWithName("Archer2")
                    self.ArcherImage.animationImages = jeremyGif?.images
                    self.ArcherImage.animationDuration = jeremyGif!.duration
                    self.ArcherImage.animationRepeatCount = 1
                    self.ArcherImage.startAnimating()
                    self.movingArrowImageView3.image = #imageLiteral(resourceName: "Arrow")
                    self.movingArrowView3.move(to: destination.applying(CGAffineTransform(translationX: 0.0, y: yPosition)), duration: 1.0, options: self.selectedCurve.animationOption)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+9) {
                    self.movingArrowImageView3.image = nil
                    cell.imgDart.image = #imageLiteral(resourceName: "Arrow")//UIImage.gifImageWithName("dartTargetGIF")
                }
            }
        }

    }
    
    // MARK: - UIBUtton Methods
    @IBAction func btnMenu(_ sender: Any) {
        self.timer?.invalidate()
    }
    @IBAction func btnOption(_ sender: Any) {
    }
    
    @IBAction func btnPlaceBid(_ sender: UIButton) {
        
        if strSelectedBid.isEmpty{
            ValidationManager.sharedManager.showAlert(message: "Select Amount", controller: self)
        }else{
            self.callWebServie_Customer_PlaceBid()
        }
    }
    @IBAction func btnBiddingOk(_ sender: Any) {
        
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewBiddingDone, viewPopUP: viewBiddingDoneIn)
        
        self.isBiddingDone = true
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3){
            self.viewBidMain.isHidden = true
        }
        self.bottomViewBid.constant = -700
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
//        self.blockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateBlockTimer), userInfo: nil, repeats: true)
//
//        self.lblBlockTitle.text = "Result in"
//        self.viewBlock.isHidden = false
    }
    
    
    // MARK: - ENSideMenu Delegate
    func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
        switch position {
            
        case FrontViewPosition.leftSideMostRemoved:
            print("LeftSideMostRemoved")
            
        case FrontViewPosition.leftSideMost:
            print("LeftSideMost")
            
        case FrontViewPosition.leftSide:
            print("LeftSide")
            
        case FrontViewPosition.left:
            print("Left")
            AppDelegate.sharedManager.navigateToStartGame()
            
        case FrontViewPosition.right:
            print("Right")
            
        case FrontViewPosition.rightMost:
            print("RightMost")
            
        case FrontViewPosition.rightMostRemoved:
            print("RightMostRemoved")
        @unknown default:
            print("Unknown")
        }
    }
    func sideMenuWillOpen() {
        self.view.isUserInteractionEnabled=false;
        print("sideMenuWillOpen")
    }
    func sideMenuWillClose() {
        self.view.isUserInteractionEnabled=true;
        print("sideMenuWillClose")
    }
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("sideMenuShouldOpenSideMenu")
        return true
    }
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }
    
    // MARK: - Swipe Gesture
    
    
    func swipeUptoBid(){
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.up
        self.viewBid.addGestureRecognizer(swipeGesture)
        
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.down
        self.viewBid.addGestureRecognizer(swipeGesture)
    }
    
    // MARK: - Webservice Methods
    func callWebServie_Customer_GetDashboard(isFromTimer:Bool = false){
        
        WebServiceManager.sharedManager.requestGet(strURL: kUrlDashboard, params: nil, success: { (response) in
            print(response)
            
            let error = response["message"] as? String
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            
            let futureGameArray = data["future_games"] as? [[String:Any]] ?? [[:]]
            let gameArray = data["game_config"] as? [[String:Any]] ?? [[:]]
            
            let past_game = data["past_game"] as? [String:Any] ?? [:]
            self.lastgameEndTime = past_game["end_time"] as? String ?? ""
            self.lastgameStartTime = past_game["start_time"] as? String ?? ""
                        
            if code == 200 || status == "success"{
                
                self.arrFutureGames.removeAll()
                for index in 0..<futureGameArray.count{
                    
                    let obj = futureGameArray[index]
                    let game = futureGamesDataModal.init(dict: obj)
                    self.arrFutureGames.append(game)
                }
                                
                self.arrColors.removeAll()
                self.arrWinString.removeAll()
                self.arrWinningColors.removeAll()
                for index in 0..<gameArray.count{
                    
                    let obj = gameArray[index]
                    let color = ColorDataModal.init(dict: obj)
                    self.arrColors.append(color)
                    self.arrWinString.append(color.color_name)
                    
                    if color.status == "Win"{
                        self.arrWinningColors.append(color.color_name)
                    }
                }
                
                self.arrWinString = self.arrWinString+self.arrWinString+self.arrWinString+self.arrWinString+self.arrWinString+self.arrWinString
                
                self.arrWin.removeAll()
                for index in 0..<self.arrWinString.count{
                    
                    var color = self.arrWinString[index]
                    if index > 29{
                        color = String(color.dropLast())
                        color = color+"1"
                    }else if index > 23{
                        color = String(color.dropLast())
                        color = color+"2"
                    }else if index > 17{
                        color = String(color.dropLast())
                        color = color+"3"
                    }
                    self.arrWin.append(UIColor(hexString: color))
                }
                
                if isFromTimer == false{
                    self.arrWin = self.arrWin.shuffled()
                    self.collectionViewWin.reloadData()
                }
                
                if isFromTimer == false || self.arrFutureGames.count == 0{
//                    self.timer?.invalidate()
//                    self.blockTimer?.invalidate()
                    self.checkWhichGameIsInCurrentTimeSlot()
                }
                
                if isFromTimer == true{
                    
                    self.setMovingArrowsToTarget()
                }
                
                self.collectionViewColors.reloadData()
                
            }else{
                ValidationManager.sharedManager.showAlert(message: error ?? "Something went wrong, please try agian", title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try again", title: "Alert", controller: self)
            
        }
    }
    
    
    func callWebServie_Customer_PlaceBid(){
        
        let dict = ["game_id": self.strSelectedGameId, "color_id": self.strSelectedColorId, "amount": self.strSelectedBid]
        
        WebServiceManager.sharedManager.requestPost(strURL: kUrlGameStart, params: dict, success: { (response) in
            print(response)
            
            let error = response["error"] as? [String]
            var errMsg = "Something went wrong, please try agian"
            if error?.count ?? 0 > 0{
                errMsg = error?[0] as? String ?? "Something went wrong, please try agian"
            }
            
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let message = response["message"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let game = data["Bid"] as? [String:Any] ?? [:]
            if code == 200 || status == "success"{
                
                print("bid done")
                
                let color = game["color_id"] as? String ?? ""
                let colorId = Int(color)
                self.arrSelectedColors.append(colorId ?? 0)
                UserDefaults.standard.set(self.arrSelectedColors, forKey: UserDefaults.Keys.arrSelectedColors)
                
                self.collectionViewColors.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                    
                    ValidationManager.sharedManager.showMainViewNN(viewMain: self.viewBiddingDone, viewInside: self.viewBiddingDoneIn)
                }
                
//                self.timer?.invalidate()
//                self.blockTimer?.invalidate()
//                self.checkWhichGameIsInCurrentTimeSlot()
                
            }else{
                ValidationManager.sharedManager.showAlert(message: errMsg, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try agian", title: "Alert", controller: self)
            
        }
    }
    
    func callWebServie_GetResult(){
        
        let dict = ["game_id": self.strSelectedGameId]
        
        WebServiceManager.sharedManager.requestPost(strURL: kUrlResultOfGame, params: dict, success: { (response) in
            print(response)
            
            let error = response["error"] as? [String]
            var errMsg = "Something went wrong, please try agian"
            if error?.count ?? 0 > 0{
                errMsg = error?[0] ?? "Something went wrong, please try agian"
            }
            
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let Bids_array = data["Bids_array"] as? [[String:Any]] ?? [[:]]
            if code == 200 || status == "success"{
                                                
                self.arrBidResultGames.removeAll()
                for index in 0..<Bids_array.count{
                    
                    let obj = Bids_array[index]
                    let result = BidsResultDataModal.init(dict: obj)
                    self.arrBidResultGames.append(result)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                    self.isShowSummeryView = true
                    self.viewSummery.isHidden = false
                }

                if self.arrBidResultGames.count > 0{
                    let obj = self.arrBidResultGames[0]
                    
                    if obj.current_status != "Loss"{
                        self.lblResultType.text = "You Won"
                    }else{
                        self.lblResultType.text = "You Lost"
                    }
                }else{
                    self.lblResultType.text = "You did Not Played"
                }
                                
//                self.timer?.invalidate()
//                self.blockTimer?.invalidate()
//                self.checkWhichGameIsInCurrentTimeSlot()
                
            }else{
                ValidationManager.sharedManager.showAlert(message: errMsg, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try agian", title: "Alert", controller: self)
            
        }
    }
}

//MARK:- Collection View Methods
extension PlayVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewBid{
            return arrBidPrice.count
        }else if collectionView == collectionViewWin{
            return arrWin.count
        }else{
            return self.arrColors.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewBid{
            let cellIdentifier = "PlayCVCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PlayCVCell
            cell.lblBidPrice.text! = arrBidPrice[indexPath.row]
            cell.viewCell.setCornerRadius(cornerRadius: 15, borderColor: UIColor.darkGray, borderWidth: 1)

            if selectedIndex == indexPath.row{
                cell.viewCell.backgroundColor = UIColor.lightGray
                cell.lblBidPrice.textColor = UIColor.white
            }else{
                cell.viewCell.backgroundColor = UIColor.white
                cell.lblBidPrice.textColor = UIColor.black
            }
            
            return cell
            
        }else if collectionView == collectionViewWin{
            let cellIdentifier = "WinCVCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! WinCVCell
            cell.viewCell.setCornerRadius(cornerRadius: 5, borderColor: UIColor.darkGray, borderWidth: 1)
            cell.viewCell.backgroundColor = arrWin[indexPath.row]
            
            cell.imgDart.image = nil
            cell.btnCell.tag = indexPath.row
            cell.btnCell.addTarget(self, action:#selector(btnViewClicked), for: .touchUpInside)

            return cell
            
        }else{
            let cellIdentifier = "Cell_ForColors"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! Cell_ForColors
            
            let obj = self.arrColors[indexPath.row]
            let color = UIColor(hexString: obj.color_name)
            cell.viewMain.backgroundColor = color
            
            cell.viewBorder.setCornerRadius(cornerRadius: 0, borderColor: nil, borderWidth: nil)
            
            if self.arrSelectedColors.contains(obj.color_id){
                cell.viewBorder.setCornerRadius(cornerRadius: 10, borderColor: .darkGray, borderWidth: 1)
            }
            
            return cell
        }
        

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewBid{
            self.lblSelectedBid.text! = arrBidPrice[indexPath.row]

            let editedText = self.lblSelectedBid.text!.replacingOccurrences(of: "₹ ", with: "")
            self.strSelectedBid = editedText
            
            self.strSelectedGameId = arrFutureGames[indexPath.row].game_id
            self.selectedIndex = indexPath.row
            self.collectionViewBid.reloadData()
        }else{
            
            let obj = self.arrColors[indexPath.row]
            if self.arrSelectedColors.contains(obj.color_id){
                return
            }
            
            self.viewBidMain.isHidden = false
            self.bottomViewBid.constant = 0
            self.strSelectedColorId = String(self.arrColors[indexPath.row].color_id)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewBid{
            let cellWidth = CGFloat((self.collectionViewBid.frame.size.width) / 4.0)
            //            let cellHeight = cellWidth * 1.5
            let cellHeight = CGFloat(43)
            
            return CGSize(width: cellWidth, height: cellHeight)
        }else if collectionView == collectionViewWin{
            let cellWidth = CGFloat((self.collectionViewWin.frame.size.width) / 4.0)
            let cellHeight = cellWidth
            //            let cellHeight = CGFloat(50)
            
            return CGSize(width: cellWidth, height: cellHeight)
        }else{
            let cellWidth = CGFloat((self.collectionViewColors.frame.size.width) / 2.0)
            let cellHeight = cellWidth * 1.3
            
            return CGSize(width: cellWidth, height: cellHeight)
        }
    }
    
    @objc func btnViewClicked(sender: UIButton) {
        
        self.view.endEditing(true)
        self.strSelectedColorIndex = sender.tag
        let destination = view.convert(sender.center, from: sender.superview)
        movingArrowView1.move(to: destination.applying(CGAffineTransform(translationX: 0.0, y: -70.0)),
                          duration: 1.0,
                          options: selectedCurve.animationOption)
        self.collectionViewWin.reloadData()
    }
}


//MARK:- Swipe Methods

extension PlayVC{
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
//        self.bottomConstantEmergencyCall = self.viewEmergencyCall.frame.size.height
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")


                DispatchQueue.main.asyncAfter(deadline: .now()+0.3){
                    self.viewBidMain.isHidden = true
                }
                self.bottomViewBid.constant = -700
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
//                    self.viewSearchBar.isHidden = true
//                }
//
//                self.ViewCreateBottom.constant = 5
//                UIView.animate(withDuration: 0.7) {
//                    self.view.layoutIfNeeded()
//                }
                
            default:
                break
            }
        }
    }
}


// MARK: AnimationCurvePickerViewControllerDelegate
//extension PlayVC: AnimationCurvePickerViewControllerDelegate {
//
//  func animationCurvePickerViewController(_ controller: AnimationCurvePickerViewController, didSelectCurve curve: UIView.AnimationCurve) {
//    selectedCurve = curve
//
//    controller.willMove(toParent: nil)
//    controller.view.removeWithZoomOutAnimation(duration: 1.0, options: selectedCurve.animationOption)
//    controller.removeFromParent()
//  }
//
//}
