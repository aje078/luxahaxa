//
//  WalletVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 04/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class WalletVC: BaseViewController , SWRevealViewControllerDelegate{

    
    @IBOutlet weak var lblBalance: UILabel!
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var viewBalance: UIView!{
        didSet{
            viewBalance.setViewShadowWithCorner(radius: 5, opacity: 0.8)
        }
    }
    @IBOutlet weak var viewSend: UIView!{
        didSet{
            viewSend.setViewShadowWithCorner(radius: 5, opacity: 0.8)
        }
    }
    @IBOutlet weak var viewReceive: UIView!{
        didSet{
            viewReceive.setViewShadowWithCorner(radius: 5, opacity: 0.8)
        }
    }
    
    
    @IBOutlet weak var viewWithdrawAmount: UIView!
    @IBOutlet weak var viewWithdrawAmountIn: UIView!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtRemark: UITextField!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    
    @IBOutlet weak var tableViewWallet: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.configureViewController()
    }
    
    @IBAction func btnMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSend(_ sender: Any) {
    }
    @IBAction func btnReceive(_ sender: Any) {
    }
    @IBAction func btnWithdrawAmount(_ sender: Any) {
        self.viewWithdrawAmount.isHidden = false
    }
    
    @IBAction func btnRequest(_ sender: Any) {
        self.view.endEditing(true)
    }
    @IBAction func btnDecline(_ sender: Any) {
        self.view.endEditing(true)
        self.viewWithdrawAmount.isHidden = true
        
    }
    @IBAction func btnClose(_ sender: Any) {
        self.view.endEditing(true)
        self.viewWithdrawAmount.isHidden = true
    }
    
    
    
    func configureViewController(){

        self.btnDecline.setCornerRadius(cornerRadius: nil, borderColor: UIColor.black, borderWidth: 1)
        self.viewWithdrawAmount.isHidden = true
    }

}

//MARK:- Table View Methods

extension WalletVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
        
        return cell
        
    }
    
}
