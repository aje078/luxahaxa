//
//  ChangePasswordVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 14/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var lblNewPassword: UILabel!
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    
    @IBOutlet weak var hViewInOldPassword: NSLayoutConstraint!
    @IBOutlet weak var hViewInNewPassword: NSLayoutConstraint!
    
    @IBOutlet weak var bLblOldPassword: NSLayoutConstraint!
    @IBOutlet weak var bLblNewPassword: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViewController()
    }
    
    func configureViewController(){
        self.height_required()

        txtOldPassword.delegate = self
        txtNewPassword.delegate = self


    }
    
    // MARK: - Text Field Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtOldPassword{
            txtNewPassword.becomeFirstResponder()
        }else if textField == txtNewPassword{
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtOldPassword{

            self.height_required()
            self.hViewInOldPassword.constant = 2
            
            self.bLblOldPassword.constant = 20
            self.lblOldPassword.font = lblOldPassword.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtNewPassword{
            self.height_required()
            self.hViewInNewPassword.constant = 2

            self.bLblNewPassword.constant = 20
            self.lblNewPassword.font = lblNewPassword.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }

        return true

    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtOldPassword{
            if self.txtOldPassword.text?.count == 0{
                self.bLblOldPassword.constant = 3
                self.lblOldPassword.font = lblOldPassword.font.withSize(17)
            }
        }else if textField == txtNewPassword{
            if self.txtNewPassword.text?.count == 0{
                self.bLblNewPassword.constant = 3
                self.lblNewPassword.font = lblNewPassword.font.withSize(17)
            }
        }
        
        return true
    }
    
    func height_required(){
        
        self.hViewInOldPassword.constant = 1
        self.hViewInNewPassword.constant = 1

        self.lblOldPassword.text! = "OLD PASSWORD"
        self.lblNewPassword.text! = "NEW PASSWORD"

        
        if self.txtOldPassword.text?.count == 0{
            self.bLblOldPassword.constant = 3
            self.lblOldPassword.font = lblOldPassword.font.withSize(17)
        }
        
        if self.txtNewPassword.text?.count == 0{
            self.bLblNewPassword.constant = 3
            self.lblNewPassword.font = lblNewPassword.font.withSize(17)
        }
        

    }

}
