//
//  AccountDetailsVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 14/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class AccountDetailsVC: BaseViewController, UITextFieldDelegate {

    var userObj:UserDataModal?
    
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblBankAccountNumber: UILabel!
    @IBOutlet weak var lblBankBranch: UILabel!
    @IBOutlet weak var lblIFSC: UILabel!
    
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtBankAccountNumber: UITextField!
    @IBOutlet weak var txtBankBranch: UITextField!
    @IBOutlet weak var txtIFSC: UITextField!
    
    @IBOutlet weak var hViewInBankName: NSLayoutConstraint!
    @IBOutlet weak var hViewInBankAccountNumber: NSLayoutConstraint!
    @IBOutlet weak var hViewInBankBranch: NSLayoutConstraint!
    @IBOutlet weak var hViewInIFSC: NSLayoutConstraint!
    
    @IBOutlet weak var bLblBankName: NSLayoutConstraint!
    @IBOutlet weak var bLblBankAccountNumber: NSLayoutConstraint!
    @IBOutlet weak var bLblBankBranch: NSLayoutConstraint!
    @IBOutlet weak var bLblIFSC: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViewController()
       
    }
    
    func configureViewController(){
        self.height_required()

        self.configureUserData()
    }
    
    func configureUserData(){

        txtBankName.delegate = self
        txtBankAccountNumber.delegate = self
        txtBankBranch.delegate = self
        txtIFSC.delegate = self
        
        self.txtBankName.text = self.userObj?.bank_name
        self.txtBankAccountNumber.text = self.userObj?.bank_account_number
        self.txtBankBranch.text = self.userObj?.bank_branch_name
        self.txtIFSC.text = self.userObj?.bank_ifsc_code
        
        self.bLblBankName.constant = 20
        self.lblBankName.font = lblBankName.font.withSize(14)
        
        self.bLblBankAccountNumber.constant = 20
        self.lblBankAccountNumber.font = lblBankAccountNumber.font.withSize(14)
        
        self.bLblBankBranch.constant = 20
        self.lblBankBranch.font = lblBankBranch.font.withSize(14)
        
        self.bLblIFSC.constant = 20
        self.lblIFSC.font = lblIFSC.font.withSize(14)
        
        if self.txtBankName.text?.count == 0{
            self.bLblBankName.constant = 3
            self.lblBankName.font = lblBankName.font.withSize(17)
        }
        
        if self.txtBankAccountNumber.text?.count == 0{
            self.bLblBankAccountNumber.constant = 3
            self.lblBankAccountNumber.font = lblBankAccountNumber.font.withSize(17)
        }
        
        if self.txtBankBranch.text?.count == 0{
            self.bLblBankBranch.constant = 3
            self.lblBankBranch.font = lblBankBranch.font.withSize(17)
        }
        
        if self.txtIFSC.text?.count == 0{
            self.bLblIFSC.constant = 3
            self.lblIFSC.font = lblIFSC.font.withSize(17)
        }
        

    }
    
    @IBAction func btnUpdateAccountDetails(_ sender: Any) {
        
        if (txtBankName.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter bank name", title: "Alert", controller: self)
        }else if (txtBankAccountNumber.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter bank account number", title: "Alert", controller: self)
        }else if (txtBankBranch.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter bank branch code", title: "Alert", controller: self)
        }else if (txtIFSC.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter IFSC code", title: "Alert", controller: self)
        }else{
            self.callWebServie_Customer_UpdateAccountDetails()
        }
    }
    
    
    // MARK: - Text Field Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtBankName{
            txtBankAccountNumber.becomeFirstResponder()
        }else if textField == txtBankAccountNumber{
            txtBankBranch.becomeFirstResponder()
        }else if textField == txtBankBranch{
            txtIFSC.becomeFirstResponder()
        }else if textField == txtIFSC{
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtBankName{

            self.height_required()
            self.hViewInBankName.constant = 2
            
            self.bLblBankName.constant = 20
            self.lblBankName.font = lblBankName.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtBankAccountNumber{
            self.height_required()
            self.hViewInBankAccountNumber.constant = 2

            self.bLblBankAccountNumber.constant = 20
            self.lblBankAccountNumber.font = lblBankAccountNumber.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtBankBranch{
            self.height_required()
            self.hViewInBankBranch.constant = 2

            self.bLblBankBranch.constant = 20
            self.lblBankBranch.font = lblBankBranch.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtIFSC{
            self.height_required()
            self.hViewInIFSC.constant = 2

            self.bLblIFSC.constant = 20
            self.lblIFSC.font = lblIFSC.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }

        return true

    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtBankName{
            if self.txtBankName.text?.count == 0{
                self.bLblBankName.constant = 3
                self.lblBankName.font = lblBankName.font.withSize(17)
            }
        }else if textField == txtBankAccountNumber{
            if self.txtBankAccountNumber.text?.count == 0{
                self.bLblBankAccountNumber.constant = 3
                self.lblBankAccountNumber.font = lblBankAccountNumber.font.withSize(17)
            }
        }else if textField == txtBankBranch{
            if self.txtBankBranch.text?.count == 0{
                self.bLblBankBranch.constant = 3
                self.lblBankBranch.font = lblBankBranch.font.withSize(17)
            }
        }else if textField == txtIFSC{
            if self.txtIFSC.text?.count == 0{
                self.bLblIFSC.constant = 3
                self.lblIFSC.font = lblIFSC.font.withSize(17)
            }
        }
        
        return true
    }
    
    func height_required(){
        
        self.hViewInBankName.constant = 1
        self.hViewInBankAccountNumber.constant = 1
        self.hViewInBankBranch.constant = 1
        self.hViewInIFSC.constant = 1
        
        self.lblBankBranch.text! = "BANK NAME"
        self.lblBankAccountNumber.text! = "BANK ACCOUNT NUMBER"
        self.lblBankBranch.text! = "BANK BRANCH NAME"
        self.lblIFSC.text! = "IFSC CODE"
        
        if self.txtBankName.text?.count == 0{
            self.bLblBankName.constant = 3
            self.lblBankName.font = lblBankName.font.withSize(17)
        }
        
        if self.txtBankAccountNumber.text?.count == 0{
            self.bLblBankAccountNumber.constant = 3
            self.lblBankAccountNumber.font = lblBankAccountNumber.font.withSize(17)
        }
        
        if self.txtBankBranch.text?.count == 0{
            self.bLblBankBranch.constant = 3
            self.lblBankBranch.font = lblBankBranch.font.withSize(17)
        }
        
        if self.txtIFSC.text?.count == 0{
            self.bLblIFSC.constant = 3
            self.lblIFSC.font = lblIFSC.font.withSize(17)
        }

    }
    
    

    
    
}

// MARK: - Webservice Methods

extension AccountDetailsVC{
    

    
    func callWebServie_Customer_UpdateAccountDetails(){
       
        let dict = ["bank_name": self.txtBankName.text!, "bank_account_number": self.txtBankAccountNumber.text!, "bank_branch_name": self.txtBankBranch.text!, "bank_ifsc_code": self.txtIFSC.text!]
        
        WebServiceManager.sharedManager.requestPost(strURL: kUrlUpdateProfile, params: dict, success: { (response) in
            print(response)
            
            let error = response["error"] as? [String]
            var errMsg = "Something went wrong, please try agian"
            if error?.count ?? 0 > 0{
                errMsg = error?[0] as? String ?? "Something went wrong, please try agian"
            }
            
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let token = data["token"] as? String ?? ""
            if code == 200 || status == "success"{
                
                print("details Updated")

                ValidationManager.sharedManager.showAlert(message: "User Profile Updated", title: "Alert", controller: self)

            }else{
                ValidationManager.sharedManager.showAlert(message: errMsg, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try agian", title: "Alert", controller: self)
            
        }
    }
    
}
