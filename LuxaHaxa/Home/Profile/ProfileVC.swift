//
//  ProfileVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 08/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit
import AlamofireImage

class ProfileVC: BaseViewController , SWRevealViewControllerDelegate{
    
    var userObj:UserDataModal?
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!{
        didSet{
            imgProfile.setCornerRadius(cornerRadius: 40, borderColor: .lightGray, borderWidth: 2)
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewGeneral: UIView!{
        didSet{
            viewGeneral.setViewShadowWithCorner(radius: 1, opacity: 0.8)
        }
    }
    @IBOutlet weak var viewOthers: UIView!{
        didSet{
            viewOthers.setViewShadowWithCorner(radius: 1, opacity: 0.8)
        }
    }
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblRecievedMoney: UILabel!
    @IBOutlet weak var lblSpentMoney: UILabel!
    @IBOutlet weak var viewBalance: UIView!{
        didSet{
            viewBalance.setCornerRadius(cornerRadius: 5, borderColor: nil, borderWidth: nil)
        }
    }
    @IBOutlet weak var viewReceived: UIView!{
        didSet{
            viewReceived.setViewShadowWithCorner(radius: 5, opacity: 0.8)
        }
    }
    @IBOutlet weak var viewSent: UIView!{
        didSet{
            viewSent.setViewShadowWithCorner(radius: 5, opacity: 0.8)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callWebServie_Customer_GetProfile()
    }
    
    func configureUserData(obj: UserDataModal){
        
        self.lblName.text = obj.name
        self.lblBalance.text = "\(obj.user_balance) ₹"
        self.lblRecievedMoney.text = "\(obj.month_buyed) ₹"
        self.lblSpentMoney.text = "\(obj.month_spent) ₹"
        
        let image = obj.profile_file
        if let url = URL(string: image){
            self.imgProfile.af.setImage(withURL: url)
        }
    }
    
    @IBAction func btnMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnupdateProfile(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        vc.userObj = self.userObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOption(_ sender: Any) {
        
    }
    
    @IBAction func btnAccountDetails(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountDetailsVC") as! AccountDetailsVC
        vc.userObj = self.userObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPassword(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        

    }
    @IBAction func btnInviteFriends(_ sender: Any) {
        let firstActivityItem = "Text you want"
        let secondActivityItem : NSURL = NSURL(string: "http//:urlyouwant")!
        // If you want to put an image
        //        let image : UIImage = UIImage(named: "image.jpg")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
        
        // This line remove the arrow of the popover to show in iPad
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    // MARK: - Webservice Methods
    func callWebServie_Customer_GetProfile(){
        
        WebServiceManager.sharedManager.requestGet(strURL: kUrlGetProfile, params: nil, success: { (response) in
            print(response)
            
            let error = response["message"] as? String
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            var token = data["token"] as? String ?? ""
            let user_balance = data["user_balance"] as? Int ?? 0
            let month_buyed = data["month_buyed"] as? Int ?? 0
            let month_spent = data["month_spent"] as? Int ?? 0

            if token == ""{
                token = UserDefaults.standard.string(forKey: UserDefaults.Keys.token) ?? ""
            }
            if code == 200 || status == "success"{
                
                let obj = UserDataModal.init(dict: user, token: token, user_balance: user_balance, month_buyed: month_buyed, month_spent: month_spent)
                self.userObj = obj
                self.configureUserData(obj: obj)
                
            }else{
                ValidationManager.sharedManager.showAlert(message: error ?? "Something went wrong, please try agian", title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try again", title: "Alert", controller: self)
            
        }
    }
    
}
