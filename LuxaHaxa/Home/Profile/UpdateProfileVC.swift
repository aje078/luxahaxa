//
//  UpdateProfileVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 10/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit
import AlamofireImage

class UpdateProfileVC: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtPanNumber: UITextField!
    @IBOutlet weak var txtAadharNumber: UITextField!
    
    @IBOutlet weak var viewInUserName: UIView!
    @IBOutlet weak var viewInEmail: UIView!
    @IBOutlet weak var viewInMobile: UIView!
    @IBOutlet weak var viewInPanNumber: UIView!
    @IBOutlet weak var viewInAadharNumber: UIView!
        
    @IBOutlet weak var hViewInUserName: NSLayoutConstraint!
    @IBOutlet weak var hViewInEmail: NSLayoutConstraint!
    @IBOutlet weak var hViewInMobile: NSLayoutConstraint!
    @IBOutlet weak var hViewInPanNumber: NSLayoutConstraint!
    @IBOutlet weak var hViewInAadharNumber: NSLayoutConstraint!

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEMail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblPanNumber: UILabel!
    @IBOutlet weak var lblAadharNumber: UILabel!

    @IBOutlet weak var bLblUserName: NSLayoutConstraint!
    @IBOutlet weak var bLblEmail: NSLayoutConstraint!
    @IBOutlet weak var bLblMobile: NSLayoutConstraint!
    @IBOutlet weak var bLblPanNumber: NSLayoutConstraint!
    @IBOutlet weak var bLblAadharNumber: NSLayoutConstraint!
    
    var imagePicker = UIImagePickerController()
    var imgUploadProfile:UIImage? = nil
    var imgUploadPanCard:UIImage? = nil
    var imgUploadAadharCard:UIImage? = nil
    
    var isFromProfilePic:Bool = false
    var isFromPanCard:Bool = false
    var isFromAadharCard:Bool = false
    
    @IBOutlet weak var imgProfile: UIImageView!{
        didSet{
            imgProfile.setCornerRadius(cornerRadius: 45, borderColor: .lightGray, borderWidth: 2)
        }
    }
    
    @IBOutlet weak var imgPanCard: UIImageView!{
        didSet{
            imgPanCard.setCornerRadius(cornerRadius: 0, borderColor: .white, borderWidth: 1)
        }
    }
    
    @IBOutlet weak var imgAadharCard: UIImageView!{
        didSet{
            imgAadharCard.setCornerRadius(cornerRadius: 0, borderColor: .white, borderWidth: 2)
        }
    }

    var userObj:UserDataModal?

    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        self.configureUserData()
        // Do any additional setup after loading the view.
    }
    
    func configureUserData(){
        
        let profile_file = self.userObj?.profile_file ?? ""
        if let url = URL(string: profile_file){
            self.imgProfile.af.setImage(withURL: url)
        }
        
        let pan_file = self.userObj?.pan_file ?? ""
        if let url = URL(string: pan_file){
            self.imgPanCard.af.setImage(withURL: url)
        }
        
        let aadhar_file = self.userObj?.aadhar_file ?? ""
        if let url = URL(string: aadhar_file){
            self.imgAadharCard.af.setImage(withURL: url)
        }
        
        self.txtUserName.text = self.userObj?.name
        self.txtEmail.text = self.userObj?.email
        self.txtMobile.text = self.userObj?.mobile
        self.txtPanNumber.text = self.userObj?.pan_number
        self.txtAadharNumber.text = self.userObj?.aadhar_number
        
        self.bLblUserName.constant = 20
        self.lblUserName.font = lblUserName.font.withSize(14)
        
        self.bLblEmail.constant = 20
        self.lblEMail.font = lblEMail.font.withSize(14)
        
        self.bLblMobile.constant = 20
        self.lblMobile.font = lblMobile.font.withSize(14)
        
        self.bLblPanNumber.constant = 20
        self.lblPanNumber.font = lblPanNumber.font.withSize(14)
        
        self.bLblAadharNumber.constant = 20
        self.lblAadharNumber.font = lblAadharNumber.font.withSize(14)
        
        if self.txtUserName.text?.count == 0{
            self.bLblUserName.constant = 3
            self.lblUserName.font = lblUserName.font.withSize(17)
        }
        
        if self.txtEmail.text?.count == 0{
            self.bLblEmail.constant = 3
            self.lblEMail.font = lblEMail.font.withSize(17)
        }
        
        if self.txtMobile.text?.count == 0{
            self.bLblMobile.constant = 3
            self.lblMobile.font = lblMobile.font.withSize(17)
        }
        
        if self.txtPanNumber.text?.count == 0{
            self.bLblPanNumber.constant = 3
            self.lblPanNumber.font = lblPanNumber.font.withSize(17)
        }
        
        if self.txtAadharNumber.text?.count == 0{
            self.bLblAadharNumber.constant = 3
            self.lblAadharNumber.font = lblAadharNumber.font.withSize(17)
        }
    }
    
    // MARK: - UIButton Methods
    @IBAction func btnUpdateProfile(_ sender: Any) {
        self.view.endEditing(true)

        if (txtUserName.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter Username", title: "Alert", controller: self)
        }else if (txtEmail.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter email", title: "Alert", controller: self)
        }else if !ValidationManager.sharedManager.isValidEmail(strEmail: txtEmail.text!){

            ValidationManager.sharedManager.showAlert(message: "Please enter valid email id", title: "Alert", controller: self)
        }else if (txtMobile.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter mobile number", title: "Alert", controller: self)
        }else if (txtAadharNumber.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter valid aadhar number", title: "Alert", controller: self)
        }else if txtAadharNumber.text!.count < 12 {

            ValidationManager.sharedManager.showAlert(message: "Aadhar number should be atleast 12 characters long", title: "Alert", controller: self)
        }else if (txtPanNumber.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter valid pan number", title: "Alert", controller: self)
        }else if imgUploadProfile == nil || self.imgProfile.image == UIImage(named: "profile_place_holder") {
            
            ValidationManager.sharedManager.showAlert(message: "Please upload profile picture", title: "Alert", controller: self)
        }else if imgUploadAadharCard == nil || self.imgAadharCard == UIImage(named: "plusIcon"){
            
            ValidationManager.sharedManager.showAlert(message: "Please upload aadhar card image", title: "Alert", controller: self)
        }else if imgUploadPanCard == nil || self.imgPanCard.image == UIImage(named: "plusIcon"){
            
            ValidationManager.sharedManager.showAlert(message: "Please upload pan card image", title: "Alert", controller: self)
        }else{
            self.callWebServie_Customer_UpdateProfile()
        }
    }
    
    @IBAction func btnUploadProfile(_ sender: Any) {
        isFromProfilePic = true
        isFromPanCard = false
        isFromAadharCard = false
        self.pick_photo()
    }
    @IBAction func btnUploadPanCard(_ sender: Any) {
        isFromProfilePic = false
        isFromPanCard = true
        isFromAadharCard = false
        self.pick_photo()
    }
    @IBAction func btnUploadAadharCard(_ sender: Any) {
        isFromProfilePic = false
        isFromPanCard = false
        isFromAadharCard = true
        self.pick_photo()
    }
    
    // MARK: - Webservice Methods
    func callWebServie_Customer_UpdateProfile(){
       
        let dict = ["name": self.txtUserName.text!, "email": self.txtEmail.text!, "mobile": self.txtMobile.text!, "aadhar_number": self.txtAadharNumber.text!, "pan_number": self.txtPanNumber.text!, "phone": self.txtMobile.text!]
        
        let imgUploadProfileData = self.imgUploadProfile?.fixedOrientation().jpegCompressedImage()
        let imgUploadProfile1 = UIImage(data: imgUploadProfileData!)
        
        let imgUploadIdData = self.imgUploadPanCard?.fixedOrientation().jpegCompressedImage()
        let imgUploadId1 = UIImage(data: imgUploadIdData!)
        
        let imgUploadLicenseData = self.imgUploadAadharCard?.fixedOrientation().jpegCompressedImage()
        let imgUploadLicense1 = UIImage(data: imgUploadLicenseData!)

        let arrImage = ["profile_file":imgUploadProfile1, "pan_file":imgUploadId1, "aadhar_file":imgUploadLicense1]
        
        WebServiceManager.sharedManager.uploadMultipartImageAPI(param: dict, arrImage: arrImage as! [String : UIImage] , URlName: BaseURL+kUrlUpdateProfile, success: { (response) in
            print(response)
            
            let status = response["status"] as? String
            let message = response["message"] as? String
            let code = response["code"] as? Int
            let data = response["data"] as? [String:Any] ?? [:]
            let avatar_url = response["avatar_url"] as? String ?? ""
            let document_url = response["document_url"] as? String ?? ""
            
            if status == "success" || code == 200{
//                let email = self.dictRegister["email"] as? String ?? ""
//                let _ = UserDataModal.init(dict: data, userTmageBaseUrl:avatar_url, documentBaseUrl: document_url)
//
//                ValidationManager.sharedObjValidation.showAlertWithAction(title: "Alert", message: ( "confirmation link sent to")+"\n\(email)" , controller: self, completion: {_ in
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
//                    self.navigationController?.pushViewController(vc, animated: true)
//                })
                
            }else{
                ValidationManager.sharedManager.showAlert(message: message ?? "Something went wrong, please try agian", title: "Alert", controller: self)
            }

        }) { (error) in

            print(error)
             
        }

    }
    
    // MARK: - Text Field Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUserName{
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail{
            txtMobile.becomeFirstResponder()
        }else if textField == txtMobile{
            txtAadharNumber.becomeFirstResponder()
        }else if textField == txtAadharNumber{
            txtPanNumber.becomeFirstResponder()
        }else if textField == txtPanNumber{
            txtPanNumber.resignFirstResponder()
        }
        
        return true
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField == txtUserName{
//
//            let currentText = txtUserName.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInUserName.constant = 2
//                self.bLblUserName.constant = 20
//                self.lblUserName.font = lblUserName.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//            }
//
//        }else if textField == txtEmail{
//
//            let currentText = txtEmail.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInEmail.constant = 2
//                self.bLblEmail.constant = 20
//                self.lblEMail.font = lblEMail.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//
//            }
//
//        }
//
//
//        return true
//
//
//    }
        
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtUserName{

            self.height_required()
            self.hViewInUserName.constant = 2
            
            self.bLblUserName.constant = 20
            self.lblUserName.font = lblUserName.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtEmail{
            self.height_required()
            self.hViewInEmail.constant = 2

            self.bLblEmail.constant = 20
            self.lblEMail.font = lblEMail.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtMobile{
            self.height_required()
            self.hViewInMobile.constant = 2

            self.bLblMobile.constant = 20
            self.lblMobile.font = lblMobile.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtAadharNumber{
            self.height_required()
            self.hViewInAadharNumber.constant = 1

            self.bLblAadharNumber.constant = 20
            self.lblAadharNumber.font = lblAadharNumber.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtPanNumber{
            self.height_required()
            self.hViewInPanNumber.constant = 2

            self.bLblPanNumber.constant = 20
            self.lblPanNumber.font = lblPanNumber.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        return true

    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtUserName{
            if self.txtUserName.text?.count == 0{
                self.bLblUserName.constant = 3
                self.lblUserName.font = lblUserName.font.withSize(17)
            }
        }else if textField == txtEmail{
            if self.txtEmail.text?.count == 0{
                self.bLblEmail.constant = 3
                self.lblEMail.font = lblEMail.font.withSize(17)
            }
        }else if textField == txtMobile{
            if self.txtMobile.text?.count == 0{
                self.bLblMobile.constant = 3
                self.lblMobile.font = lblMobile.font.withSize(17)
            }
        }else if textField == txtAadharNumber{
            if self.txtAadharNumber.text?.count == 0{
                self.bLblAadharNumber.constant = 3
                self.lblAadharNumber.font = lblAadharNumber.font.withSize(17)
            }
        }else if textField == txtPanNumber{
            if self.txtPanNumber.text?.count == 0{
                self.bLblPanNumber.constant = 3
                self.lblPanNumber.font = lblPanNumber.font.withSize(17)
            }
        }
        
        return true
    }
    
    func height_required(){
        
        self.hViewInUserName.constant = 1
        self.hViewInEmail.constant = 1
        self.hViewInMobile.constant = 1
        self.hViewInPanNumber.constant = 1
        self.hViewInAadharNumber.constant = 1
        
        self.lblUserName.text! = "USERNAME"
        self.lblEMail.text! = "EMAIL"
        self.lblMobile.text! = "MOBILE"
        self.lblPanNumber.text! = "PAN CARD NUMBER"
        self.lblAadharNumber.text! = "AADHAR CARD NUMBER"
        
        if self.txtUserName.text?.count == 0{
            self.bLblUserName.constant = 3
            self.lblUserName.font = lblUserName.font.withSize(17)
        }
        
        if self.txtEmail.text?.count == 0{
            self.bLblEmail.constant = 3
            self.lblEMail.font = lblEMail.font.withSize(17)
        }
        
        if self.txtMobile.text?.count == 0{
            self.bLblMobile.constant = 3
            self.lblMobile.font = lblMobile.font.withSize(17)
        }
        
        if self.txtAadharNumber.text?.count == 0{
            self.bLblAadharNumber.constant = 3
            self.lblAadharNumber.font = lblAadharNumber.font.withSize(17)
        }
        
        if self.txtPanNumber.text?.count == 0{
            self.bLblPanNumber.constant = 3
            self.lblPanNumber.font = lblPanNumber.font.withSize(17)
        }
    }
}

// MARK: - Image Picker Methods
extension UpdateProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func pick_photo(){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        
        if isFromProfilePic == true{
            imgProfile.image = image
            self.imgUploadProfile = image
        }else if isFromPanCard == true{
            imgPanCard.image = image
            self.imgUploadPanCard = image
        }else if isFromAadharCard == true{
            imgAadharCard.image = image
            self.imgUploadAadharCard = image
        }
        
        
        //            self.imgProfile = UIImagePNGRepresentation(imgProfile.image!) as UIImage
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
}
