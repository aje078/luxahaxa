//
//  MyMenuVC.swift
//  QbeeCabs Passenger
//
//  Created by Vijay on 11/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class MyMenuVC: BaseViewController {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewForSide: UIView!
    
    @IBOutlet weak var viewDashboard: UIView!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var viewWallet: UIView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewLogOut: UIView!
    @IBOutlet weak var viewSupport: UIView!
      
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblOrder: UILabel!
    @IBOutlet weak var lblReceipient: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewHome.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.lblUserName.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.name) ?? ""
        self.lblEmail.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.email) ?? ""
//        let image = UserDefaults.standard.string(forKey: UserDefaults.Keys.userProfileImage) ?? ""
//
//        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
//            if let url = URL(string: image){
//                self.imgProfile.af.setImage(withURL: url)
//            }
//        }
    }

    @IBAction func btnDashboard(_ sender: Any) {
        
        self.configureViewcontroller()
        self.viewDashboard.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        AppDelegate.sharedManager.navigateToHome()
    }
    
    @IBAction func btnHome(_ sender: Any) {
        
        self.configureViewcontroller()
        self.viewHome.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        AppDelegate.sharedManager.navigateToStartGame()
    }
    
    @IBAction func btnLogOut(_ sender: Any) {
        
        self.configureViewcontroller()
        self.viewLogOut.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        UserDefaults.standard.set("", forKey: UserDefaults.Keys.token)
        UserDefaults.standard.set("", forKey: UserDefaults.Keys.id)
        
        AppDelegate.sharedManager.navigateToLogin()
        self.callWebServie_Customer_Logout()
    }
    
    @IBAction func btnWallet(_ sender: Any) {
      
        self.configureViewcontroller()
        self.viewWallet.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        
        self.configureViewcontroller()
        self.viewProfile.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func configureViewcontroller(){
        
        self.viewDashboard.backgroundColor = .clear
        self.viewHome.backgroundColor = .clear
        self.viewWallet.backgroundColor = .clear
        self.viewProfile.backgroundColor = .clear
        self.viewLogOut.backgroundColor = .clear
    }
    
    // MARK: - Webservice Methods
    func callWebServie_Customer_Logout(){
       
        let dict = [String:Any]()
        
        WebServiceManager.sharedManager.requestPost(strURL: kUrlLogOut, params: dict, success: { (response) in
            print(response)
            
            let error = response["message"] as? String
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let token = data["token"] as? String ?? ""
            if code == 200 || status == "success"{
                
                UserDefaults.standard.set("", forKey: UserDefaults.Keys.token)
                UserDefaults.standard.set("", forKey: UserDefaults.Keys.id)
                
                AppDelegate.sharedManager.navigateToLogin()

            }else{
                ValidationManager.sharedManager.showAlert(message: error ?? "Something went wrong, please try agian", title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try again", title: "Alert", controller: self)
            
        }
    }
}


