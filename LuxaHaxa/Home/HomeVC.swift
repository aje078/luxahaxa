//
//  HomeVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 04/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class HomeVC: BaseViewController {

    @IBOutlet weak var viewBtnPlay: UIView!
    @IBOutlet weak var viewBtnCredits: UIView!
    @IBOutlet weak var viewBtnQuit: UIView!
    
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var viewQuit: UIView!
    @IBOutlet weak var viewQuitIn: UIView!
    
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
        
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViewController()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let colorArray = [#colorLiteral(red: 0.9960071445, green: 0.6829891205, blue: 0.1235066876, alpha: 1), #colorLiteral(red: 1, green: 0.9334716201, blue: 0.4456378818, alpha: 1)]
        let startPoint = CGPoint(x: 0, y: 0.5)
        let endPoint = CGPoint(x: 1, y: 0.5)
        self.viewBtnPlay.addGradientLayerWith(colors: colorArray, startPoint: startPoint, endPoint: endPoint, locations: [0.2, 1])
        self.viewBtnCredits.addGradientLayerWith(colors: colorArray, startPoint: startPoint, endPoint: endPoint, locations: [0.2, 1])
        self.viewBtnQuit.addGradientLayerWith(colors: colorArray, startPoint: startPoint, endPoint: endPoint, locations: [0.2, 1])
        
        let backColors = [#colorLiteral(red: 0.998781383, green: 0.4456522465, blue: 0.07103154808, alpha: 1), #colorLiteral(red: 0.9962952733, green: 0.9840196967, blue: 0.1924864948, alpha: 1), #colorLiteral(red: 0.08275685459, green: 0.9831508994, blue: 0.1892304718, alpha: 1), #colorLiteral(red: 0.005358107854, green: 0.9566281438, blue: 0.9981583953, alpha: 1), #colorLiteral(red: 0.09494764358, green: 0.1679126322, blue: 0.9860342145, alpha: 1), #colorLiteral(red: 0.8442218304, green: 0.1931146681, blue: 0.9864082932, alpha: 1), #colorLiteral(red: 0.9978324771, green: 0.1661100686, blue: 0.2868427932, alpha: 1)]
        self.viewMain.addGradientLayerWith(colors: backColors, startPoint: CGPoint(x: 0.8, y: 0), endPoint: CGPoint(x: 0.5, y: 1), locations: [0 ,0.15 ,0.32, 0.5, 0.65, 0.8, 1])

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    

    @IBAction func btnPlay(_ sender: Any) {
        
        AppDelegate.sharedManager.navigateToStartGame()
    }
    
    @IBAction func btnCredits(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreditAmountVC") as! CreditAmountVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnQuit(_ sender: Any) {
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewQuit, viewInside: viewQuitIn)
    }

    func configureViewController(){
        
        self.viewQuit.isHidden = true
        self.viewQuitIn.setCornerRadius(cornerRadius: 15, borderColor: UIColor.black, borderWidth: nil)

        self.viewBtnPlay.setCornerRadius(cornerRadius: 20, borderColor: UIColor.black, borderWidth: nil)
        self.viewBtnCredits.setCornerRadius(cornerRadius: 20, borderColor: UIColor.black, borderWidth: nil)
        self.viewBtnQuit.setCornerRadius(cornerRadius: 20, borderColor: UIColor.black, borderWidth: nil)

        self.btnNo.setCornerRadius(cornerRadius: 5, borderColor: UIColor.black, borderWidth: nil)
        self.btnYes.setCornerRadius(cornerRadius: 5, borderColor: UIColor.black, borderWidth: nil)

    }
    
    @IBAction func btnNo(_ sender: Any) {
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewQuit, viewPopUP: viewQuitIn)
    }
    
    @IBAction func btnYes(_ sender: Any) {
                
        UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            exit(0)
        }
    }
    

    
    
}

extension UIView{
    
    func addGradientLayerWith(colors: [UIColor], startPoint: CGPoint?, endPoint: CGPoint?, locations: [NSNumber]?){
        let layer = CAGradientLayer()
        var layerColors = [CGColor]()
        
        for color in colors {
            layerColors.append(color.cgColor)
        }
        layer.colors = layerColors
        if startPoint != nil {
            layer.startPoint = startPoint!
        }
        if endPoint != nil {
            layer.endPoint = endPoint!
        }
        if locations == nil {
            layer.locations = [0, 1]
        }else{
            layer.locations = locations
        }
        layer.frame = self.bounds
        self.layer.insertSublayer(layer, at: 0)
    }
    
    func gradiantColorLayer(gradiantColors: [UIColor]) -> CAGradientLayer{
        let layer = CAGradientLayer()
        var colors = [CGColor]()
        for color in gradiantColors {
            colors.append(color.cgColor)
        }
        layer.colors = colors
        layer.locations = [0.5, 1]
        layer.startPoint = CGPoint(x: 0, y: 0.7)
        layer.endPoint = CGPoint(x: 0.1, y: 0)
        
        if #available(iOS 13, *), UITraitCollection.current.userInterfaceStyle == .dark {
            layer.locations = [0.2, 1]
            layer.startPoint = CGPoint(x: 0, y: 0)
            layer.endPoint = CGPoint(x: 0.7, y: 1)
        }
        
        layer.frame = self.bounds

        return layer
    }
}
