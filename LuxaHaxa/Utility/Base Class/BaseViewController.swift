//
//  BaseViewController.swift
//  Drive2uApp
//
//  Created by Vijay on 29/07/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var lblHeaderName: UILabel!{
        didSet{
            lblHeaderName.textColor = .white
//            lblHeaderName.font = UIFont(name: "System-Bold", size: 20)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
