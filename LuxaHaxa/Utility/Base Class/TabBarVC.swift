//
//  TabBarViewController.swift
//  GetU
//
//  Created by MINDIII on 9/25/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

var selected_TabIndex:Int = 2
var imageUnread = UIImageView()
var lblUnread = UILabel()

var viewOverTabBar = UIView()

let SEPARATOR_WIDTH = 1.0

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for viewController in self.viewControllers! {
            _ = viewController.view
            
        }
        self.selectedIndex = selected_TabIndex
        self.tabBar.backgroundColor = UIColor.white
        
        addUnderlinInTabBarItem()
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 1.0)
        // use for the border.
        topBorder.backgroundColor = UIColor.gray.cgColor
        tabBar.layer.addSublayer(topBorder)
        
        lblUnread = UILabel(frame: CGRect(x: (tabBar.frame.size.width - (tabBar.frame.size.width/5) - 12) , y: 5, width: 12, height: 12))
        
        lblUnread.layer.cornerRadius = 6
        lblUnread.layer.masksToBounds = true
        self.tabBar.addSubview(lblUnread)
        lblUnread.backgroundColor = #colorLiteral(red: 0.0007728835917, green: 0.2735484242, blue: 0.02437255159, alpha: 1)
        lblUnread.isHidden = true
        
        viewOverTabBar = UIView(frame: CGRect(x: 0 , y: 0, width: tabBar.frame.size.width, height: 100))
        self.tabBar.addSubview(viewOverTabBar)
        viewOverTabBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewOverTabBar.alpha = 0.4
        
        viewOverTabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func drawTabbar() {
        self.tabBar.items?[0].image = #imageLiteral(resourceName: "inactive_post_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].image = #imageLiteral(resourceName: "inactive_message_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].image = #imageLiteral(resourceName: "inactive_notifications_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[3].image = #imageLiteral(resourceName: "inactive_setting_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[4].image = #imageLiteral(resourceName: "inactive_logout_icon").withRenderingMode(.alwaysOriginal)
        
        self.tabBar.items?[0].selectedImage = #imageLiteral(resourceName: "active_post_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].selectedImage = #imageLiteral(resourceName: "active_message_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].selectedImage = #imageLiteral(resourceName: "active_notifications_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[3].selectedImage = #imageLiteral(resourceName: "active_setting_icon").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[4].selectedImage = #imageLiteral(resourceName: "active_logout_icon").withRenderingMode(.alwaysOriginal)
    }
    
    func setupTabBarSeparators(){
        let itemWidth = floor(self.tabBar.frame.size.width / CGFloat(self.tabBar.items!.count))
        let bgView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: tabBar.frame.size.height))
        
        for i in 0..<(tabBar.items?.count)! - 1{
            let separator = UIView(frame: CGRect(x: itemWidth * CGFloat(i + 1) - CGFloat(SEPARATOR_WIDTH / 2), y: 10, width: CGFloat(SEPARATOR_WIDTH), height: self.tabBar.frame.size.height-20))
            separator.backgroundColor = UIColor(red: 98.0 / 255.0, green: 98.0 / 255.0, blue: 98.0 / 255.0, alpha: 1)
            bgView.addSubview(separator)
        }
        
        UIGraphicsBeginImageContext(bgView.bounds.size)
        bgView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let tabBarBackground = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UITabBar.appearance().backgroundImage = tabBarBackground
    }
    
    func addUnderlinInTabBarItem() {
        let tabBar: UITabBar? = self.tabBar
        let divide = 5.0
        
        let view = UIView(frame: CGRect(x: (tabBar?.frame.origin.x)!, y: (tabBar?.frame.origin.y)!, width: self.view.frame.size.width/CGFloat(divide), height: 58))
        
        let border = UIImageView(frame: CGRect(x: view.frame.origin.x + 5, y: view.frame.size.height - 8, width: self.view.frame.size.width / CGFloat(divide) - 10, height: 0))
        border.backgroundColor = #colorLiteral(red: 0.0007728835917, green: 0.2735484242, blue: 0.02437255159, alpha: 1)
        
        view.addSubview(border)
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let tabBarBackground = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.tabBar.tintColor = #colorLiteral(red: 0.0007728835917, green: 0.2735484242, blue: 0.02437255159, alpha: 1)
        
        //bottom line
        tabBar?.selectionIndicatorImage = tabBarBackground
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let items = tabBar.items else { return }
        print("the selected index is : \(String(describing: items.index(of: item)))")
    
    }
}

