//
//  webserviceConstants.swift
//  
//
//  Created by mindiii on 10/30/17.
//  Copyright © 2017 mindiii. All rights reserved.
//

import Foundation

//var BaseURL : String = "http://aryumoney.com/api/"
var BaseURL : String = "http://13.127.177.177/api/"

let kUrlSignIn : String = "login"
let kUrlSignUp : String = "register"
let kUrlGoogleSignIn : String = "google"
let kUrlFacebookSignIn : String = "facebook"
let kUrlLogOut : String = "logout"
let kUrlTermsAndCondition : String = "terms"
let kUrlDashboard : String = "dashboard"
let kUrlGetProfile : String = "get_profile"
let kUrlUpdateProfile : String = "update_profile"
let kUrlGameStart : String = "game_start"
let kUrlBuy_money : String = "buy_money"
let kUrlResultOfGame : String = "result"



