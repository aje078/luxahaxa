//
//  WebServiceClass.swift
//  Link
//
//  Created by MINDIII on 10/3/17.
//  Copyright © 2017 MINDIII. All rights reserved.

// MARK: - Required things
//  vartall Alamofire with cocoapods * pod 'Alamofire'
//  install SwiftyJSON with cocoapods * pod 'SwiftyJSON'


import UIKit
import KRProgressHUD
import Alamofire
import SwiftyJSON

class WebServiceManager: NSObject {
    
    static let sharedManager = WebServiceManager()
    
    func changeParamToURL(url: String, param: [String:Any]) -> String {
        var url = url
        var index = -1
        for obj in param {
            index = index+1
            let key = obj.key
            let value = obj.value
            if index == 0 {  url = "\(url)\(key)=\(value)" } else {  url = "\(url)&\(key)=\(value)" } }
        return url
    }
    
    public func requestGet(strURL:String, params : [String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        self.startAnimating()
        
        let url = BaseURL+strURL
        print("URL = \(url)")
        let token = UserDefaults.standard.string(forKey: UserDefaults.Keys.token) ?? ""
        let headers: HTTPHeaders = [.authorization(bearerToken: token)]

        AF.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
                        
            switch responseObject.result {
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                }catch{
                    if let error : Error = responseObject.error{
                        failure(error)
                        let str = String(decoding:  responseObject.data!, as: UTF8.self)
                        print("PHP ERROR : \(str)")
                    }

                }

                self.stopAnimating()
            case .failure(let error):
                
                print(error)
                if let error : Error = responseObject.error{
                    failure(error)
                    if let error = responseObject.data{
                        let str = String(decoding:  error, as: UTF8.self)
                        print("PHP ERROR : \(str)")
                    }
                }
                self.stopAnimating()
            }
        }
    }
    
    public func requestPost(strURL:String, params : [String : Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        self.startAnimating()
        
        let url = BaseURL+strURL
        print("URL = \(url)")
        let token = UserDefaults.standard.string(forKey: UserDefaults.Keys.token) ?? ""
        let headers: HTTPHeaders = [.authorization(bearerToken: token)]

        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            switch responseObject.result {
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                self.stopAnimating()
                
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                }catch{
                    if let error : Error = responseObject.error{
                        failure(error)
                        let str = String(decoding:  responseObject.data!, as: UTF8.self)
                        print("PHP ERROR : \(str)")
                    }

                }

            case .failure(let error):
                print(error)
                
                if let error : Error = responseObject.error{
                    failure(error)
                    if let error = responseObject.data{
                        let str = String(decoding:  error, as: UTF8.self)
                        print("PHP ERROR : \(str)")
                    }
                }
                self.stopAnimating()
            }
        }
    }
    
    func uploadMultipartImageAPI(param:[String: Any], arrImage: [String : UIImage], URlName:String, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void){
        self.startAnimating()
        
        var token:String?
        if let tokens = UserDefaults.standard.value(forKey: UserDefaults.Keys.token) as? String {
            token = tokens
        }
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in param {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
            
            for (key, value) in arrImage {
                
                guard let imgData = value.jpegData(compressionQuality: 1) else { return }
                multipartFormData.append(imgData, withName: key, fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            
            
        },to: URlName, usingThreshold: UInt64.init(),
          method: .post,
          headers: nil).responseJSON{ responseObject in
            
            print(responseObject.result)
            
            switch responseObject.result {
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                }catch{
                    if let error : Error = responseObject.error{
                        failure(error)
                        let str = String(decoding:  responseObject.data!, as: UTF8.self)
                        print("PHP ERROR : \(str)")
                    }
                    
                }
                
                self.stopAnimating()

            case .failure(let error):
                
                print(error)
                
                if let error : Error = responseObject.error{
                    failure(error)
                    if let error = responseObject.data{
                        let str = String(decoding:  error, as: UTF8.self)
                        print("PHP ERROR : \(str)")
                    }
                }
                
                self.stopAnimating()
            }
        }
    }

    public func requestPostWithURLAppending(strURL:String, params : [String:Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        self.startAnimating()
        let searchURL = URL(string: strURL )
       
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print(response.result)

            switch response.result {
            case .success(let _):
                self.stopAnimating()
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    
                    // Session Expire
                    let dict = dictionary as! Dictionary<String, Any>
                    if dict["status"] as? String ?? "" != "success"{
                        if let msg = dict["message"] as? String{
                            print(msg)
                        }
                    }
                    print("\n response = \(dictionary)")
                    success(dictionary as! Dictionary<String, Any>)
                    
                }catch{
                    
                }
            case .failure(let error):
                failure(error)
               self.stopAnimating()
            }
        }
    }
    
    //Activity Indicator Method
    func startAnimating()
    {
        DispatchQueue.main.async(execute: {() -> Void in
        KRProgressHUD.show()
        })
    }
    func stopAnimating()
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            KRProgressHUD.dismiss()
        }
    }
    
    
    
}





