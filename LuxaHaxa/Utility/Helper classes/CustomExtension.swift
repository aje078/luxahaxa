//
//  CustomExtension.swift
//  Mualab
//
//  Created by MINDIII on 11/1/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

let colors:UIColor = #colorLiteral(red: 0.4611930475, green: 0.4611930475, blue: 0.4611930475, alpha: 1)
class CustomExtension: NSObject {

}

class Colors {
    var gl:CAGradientLayer!
    
    init() {
        let colorTop = UIColor.black.cgColor
        let colorBottom = UIColor.clear.cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [-0.5, 1.0]
    }
}

//MARK:- UIImageView Extension
extension UIImageView
{
    func setImageProperty() {
        layer.backgroundColor = UIColor.clear.cgColor
        layer.borderWidth = 1.5
        //self.layer.borderColor=[commenColorRed CGColor];
    }
    
    func setImageFream() {
        layer.cornerRadius = layer.frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 2.0
        layer.borderColor = colors.cgColor
    }
}

//MARK:- UIView Extension
extension UIView{
    
    func setCornerRadius(cornerRadius:CGFloat?, borderColor:UIColor?, borderWidth:CGFloat?){
        
        var radius:CGFloat = 0
        if cornerRadius != nil{
            radius = cornerRadius ?? 0
        }
        layer.cornerRadius = radius
        layer.masksToBounds = true
        
        var color = UIColor.lightGray.cgColor
        if borderColor != nil{
            color = borderColor?.cgColor ?? UIColor.lightGray.cgColor
        }
        layer.borderColor = color
        
        var width:CGFloat = 0.0
        if borderWidth != nil{
            width = borderWidth ?? 0.0
        }
        layer.borderWidth = width
        
        
    }
    
    func addshadow(top: Bool, left: Bool, bottom: Bool,  right: Bool, shadowRadius: CGFloat = 2.0, shadowColor: UIColor = .gray, shadowOpecity: Float = 1.0,roundedRadius: CGFloat = 0)  {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpecity
        self.layer.shadowColor = shadowColor.cgColor
        
        var path = UIBezierPath()
        
        var x: CGFloat = 0
        var y: CGFloat = 0
        var viewWidth = self.frame.width
        var viewHeight = self.frame.height
        
        // here x, y, viewWidth, and viewHeight can be changed in
        // order to play around with the shadow paths.
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        
        if roundedRadius > 0 {
            path = UIBezierPath(roundedRect: CGRect(x: x, y: y, width: viewWidth, height: viewHeight), cornerRadius: roundedRadius)
        } else {
            // selecting top most point
            path.move(to: CGPoint(x: x, y: y))
            // Move to the Bottom Left Corner, this will cover left edges
            
            path.addLine(to: CGPoint(x: x, y: viewHeight))
            // Move to the Bottom Right Corner, this will cover bottom edge
            
            path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
            // Move to the Top Right Corner, this will cover right edge
            
            path.addLine(to: CGPoint(x: viewWidth, y: y))
            // Move back to the initial point, this will cover the top edge
        }
        
        path.close()
        self.layer.shadowPath = path.cgPath
    }
    
    func setViewShadow(_ opacity: Float) {
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height + 1))
        //   self.backgroundColor     = [UIColor whiteColor];
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = opacity
        layer.shadowPath = shadowPath.cgPath
    }
    
    func setViewShadowWithCorner(radius: CGFloat, opacity: Float) {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowOpacity = opacity
        layer.cornerRadius = radius
    }
    
    func setShadow3D(_ opacity: Float) {
        let shadowPath = UIBezierPath(rect: CGRect(x: 2, y: 2, width: frame.size.width - 2, height: frame.size.height - 2))
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = opacity
        layer.shadowPath = shadowPath.cgPath
    }
        
    func setViewShadowWithCorner() {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowOpacity = 0.7
        layer.cornerRadius = 10
    }
    
    func setViewShadow() {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowOpacity = 0.7
        layer.cornerRadius = 10
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

public extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPhone12,8":                              return "iPhone SE (2nd generation)"
            case "iPhone13,1":                              return "iPhone 12 mini"
            case "iPhone13,2":                              return "iPhone 12"
            case "iPhone13,3":                              return "iPhone 12 Pro"
            case "iPhone13,4":                              return "iPhone 12 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                    return "iPad (8th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                    return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                    return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "AudioAccessory5,1":                       return "HomePod mini"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()

}

// MARK:- Clean Value on Primitives
extension CGFloat {
    
    public static func random(lower: CGFloat, upper: CGFloat) -> CGFloat {
        let r = CGFloat(arc4random()) / CGFloat(UInt32.max)
        return (r * (upper - lower)) + lower
    }
    
    func cleanValue(to places: Int = 0) -> String {
        let value  = String(format: "%.\(places)f", self)
        let reminder = Float(value)?.truncatingRemainder(dividingBy: 1)
        return reminder == 0 ? String(format: "%.0f", self) : String(format: "%.\(places)f", self)
    }
}

extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }

        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            // Change `Int` in the next line to `IndexDistance` in < Swift 4.1
            let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}

// MARK:- UIColor
extension UIColor {
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
    convenience init(red: Int, green: Int, blue: Int, withAlpha alpha: CGFloat) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
    convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff, withAlpha: 1.0)
    }
    
    convenience init(hexString: String) {
        self.init(hexString: hexString, alpha: 1.0)
    }
    
    convenience init(hexString: String, alpha: CGFloat) {
        var cString = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = String(cString[cString.index(cString.startIndex, offsetBy: 1) ..< cString.endIndex])
//            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }
        
        if (cString.count != 6) {
            self.init(hex: 0xf0f0f0)
        }else {
            var scannedHex:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&scannedHex)
            
            let hex = Int(scannedHex)
            self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff, withAlpha: alpha)
        }
    }

    var randomGray: UIColor {
        let colorValue = CGFloat(arc4random()%255)/255
        return UIColor(red: colorValue, green: colorValue, blue: colorValue, alpha: 1.0)
    }
    
    var randomColor: UIColor {
        return UIColor(red: CGFloat(arc4random()%255)/255, green: CGFloat(arc4random()%255)/255, blue: CGFloat(arc4random()%255)/255, alpha: 1.0)
    }
    
    class func randomColors(randomAlpha randomApha:Bool = false)->UIColor{
        let redValue = CGFloat(arc4random_uniform(255)) / 255.0
        let greenValue = CGFloat(arc4random_uniform(255)) / 255.0
        let blueValue = CGFloat(arc4random_uniform(255)) / 255.0
        let alphaValue = randomApha ? CGFloat(arc4random_uniform(255)) / 255.0 : 1
        return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: alphaValue)
    }
    
    class func randomMutedColors(randomAlpha randomApha:Bool = false)->UIColor{
        let redValue = CGFloat.random(lower: 100.0,upper: 210.0) / 255.0
        let greenValue = CGFloat.random(lower: 100.0,upper: 210.0) / 255.0
        let blueValue = CGFloat.random(lower: 100.0,upper: 210.0) / 255.0
        let alphaValue = randomApha ? CGFloat(arc4random_uniform(255)) / 255.0 : 1
        return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: alphaValue)
    }
    static func blend(color1: UIColor, intensity1: CGFloat = 0.5, color2: UIColor, intensity2: CGFloat = 0.5) -> UIColor {
        let total = intensity1 + intensity2
        let l1 = intensity1/total
        let l2 = intensity2/total
        guard l1 > 0 else { return color2}
        guard l2 > 0 else { return color1}
        var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        
        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        return UIColor(red: l1*r1 + l2*r2, green: l1*g1 + l2*g2, blue: l1*b1 + l2*b2, alpha: l1*a1 + l2*a2)
    }
}


extension UIPanGestureRecognizer {
    
    enum GestureDirection {
        case Up
        case Down
        case Left
        case Right
    }
    
    /// Get current vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func verticalDirection(target target: UIView) -> GestureDirection {
        return self.velocity(in: target).y > 0 ? .Down : .Up
    }
    
    /// Get current horizontal direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func horizontalDirection(target target: UIView) -> GestureDirection {
        return self.velocity(in: target).x > 0 ? .Right : .Left
    }
    
    /// Get a tuple for current horizontal/vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func versus(target target: UIView) -> (horizontal: GestureDirection, vertical: GestureDirection) {
        return (self.horizontalDirection(target: target), self.verticalDirection(target: target))
    }
    
    
    
    func encode(_ s: String) -> String {
        // let data = s.data(using: .utf8)
        //return String(data: data!, encoding: .utf8) ?? ""
        if let data = s.data(using: .utf8) {
            return String(data: data, encoding: .utf8) ?? ""
        }else{
            return s
        }
    }
    
}

extension String {
    public func isImageType() -> Bool {
        // image formats which you want to check
        let imageFormats = ["gif"]
        
        if URL(string: self) != nil  {
            
            let extensi = (self as NSString).pathExtension
            
            return imageFormats.contains(extensi)
        }
        return false
    }
}

extension URL {
    public var queryParameters: [String: String]? {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
            return nil
        }
        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }
        return parameters
    }
}

extension UIViewController{
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    //MARK: - Convert date formate
    func converteDateIntoNewFormate(strDateFromServer: String) -> (String) {
        
        var strConvertedDate : String = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let dateFromServer: Date? = dateFormatter.date(from: strDateFromServer)
        
        if let dateFromServer = dateFromServer {
            
            dateFormatter.dateFormat = "dd MMM yyyy"
            
            let strDate:String? = dateFormatter.string(from: dateFromServer)
            
            if let strDate = strDate {
                strConvertedDate = strDate
            }
        }
        return strConvertedDate
    }
    
    //MARK: - Convert Time formate
    func converteTimeIntoNewFormate(strTimeFromServer: String) -> (String) {
        
        var strConvertedTime : String = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        
        let timeFromServer: Date? = dateFormatter.date(from: strTimeFromServer)
        
        if let timeFromServer = timeFromServer {
            
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.dateFormat = "hh:mm a"
            //dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!

            let strTime:String? = dateFormatter.string(from: timeFromServer)
            
            if let strTime = strTime {
                strConvertedTime = strTime
            }
        }
        return strConvertedTime
    }
    
    func matchesTwoDate(dateA:Date) -> Bool{
        let currentDate = Date()
        let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
        let SystemTimeZone = NSTimeZone.system as NSTimeZone
        let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
        let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
        let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
        let TodayDate = Date(timeInterval: interval, since: currentDate)
        
        let dateB:Date = TodayDate
        var addIndex = false
        
        switch dateA.compare(dateB) {
            
        case .orderedAscending:
            addIndex = true
            print("Date A is later than date B")
        case .orderedDescending:
            addIndex = false
            print("Date A is earlier than date B")
        case .orderedSame:
            addIndex = false
            print("The two dates are the same")
        }
        return addIndex
    }
}

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}

extension String {
    
    var pairs: [String] {
        var result: [String] = []
        let characters = Array(self)
        stride(from: 0, to: characters.count, by: 2).forEach {
            result.append(String(characters[$0..<min($0+2, characters.count)]))
        }
        return result
    }
    
    mutating func insert(separator: String, every n: Int) {
        self = inserting(separator: separator, every: n)
    }
    
    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self)
        stride(from: 0, to: characters.count, by: n).forEach {
            result += String(characters[$0..<min($0+n, characters.count)])
            if $0+n < characters.count {
                result += separator
            }
        }
        return result
    }
    
    func condensingWhitespace() -> String {
        return self.components(separatedBy: .whitespacesAndNewlines)
            .filter { !$0.isEmpty }
            .joined(separator: " ")
    }

}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

//MARK:- Image rotation extension code
extension UIImage {
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.2
        case medium  = 0.4
        case high    = 0.6
        case highest = 0.8
        case Original = 1
    }
    
    func jpegCompressedImage() -> Data? {
        
        let img =  self.ResizeImage(image: self, targetSize: CGSize.init(width: 800, height: 800))
        
        var imageData : Data?
        let imgData: NSData = NSData(data: (self).jpegData(compressionQuality: 1)!)
        var imageSize = imgData.length/1024
        
        if imageSize > 3072{
            imageData = img.jpegData(compressionQuality: JPEGQuality.high.rawValue)
        }else if imageSize > 2048{
            imageData = img.jpegData(compressionQuality: JPEGQuality.highest.rawValue)
        }else{
            imageData = img.jpegData(compressionQuality: JPEGQuality.Original.rawValue)
        }
        
        let dataNew : NSData = imageData! as NSData
        imageSize = dataNew.length/1024
        return imageData
    }
    
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize.init(width: size.width*widthRatio, height: size.height*widthRatio)
        } else {
            newSize = CGSize.init(width: size.width*widthRatio, height: size.height*widthRatio)
            //newSize = CGSize.init(width: size.width, height: size.height)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
        
    // return image masked with the given color
    func maskedImage(withColor color: UIColor) -> UIImage {
        let rect = CGRect(origin: CGPoint.zero, size: self.size)
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        
        // translate/flip the graphics context (for transforming from CG coords to UI coords
        context?.translateBy(x: 0, y: rect.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        context?.clip(to: rect, mask: self.cgImage!)
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func fixedOrientation() -> UIImage {
        // No-op if the orientation is already correct
        if (imageOrientation == UIImage.Orientation.up) {
            return self
        }
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform:CGAffineTransform = CGAffineTransform.identity
        
        if (imageOrientation == UIImage.Orientation.down
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        }
        
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2))
        }
        
        if (imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            
            transform = transform.translatedBy(x: 0, y: size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi/2));
        }
        
        if (imageOrientation == UIImage.Orientation.upMirrored
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if (imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            
            transform = transform.translatedBy(x: size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx:CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                                      bitsPerComponent: cgImage!.bitsPerComponent, bytesPerRow: 0,
                                      space: cgImage!.colorSpace!,
                                      bitmapInfo: cgImage!.bitmapInfo.rawValue)!
        
        ctx.concatenate(transform)
        
        
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored
            ) {
            
            
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.height,height:size.width))
            
        } else {
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.width,height:size.height))
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg:CGImage = ctx.makeImage()!
        let imgEnd:UIImage = UIImage(cgImage: cgimg)
        
        return imgEnd
    }
}

func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
    
    let calendar = NSCalendar.current
    let now = NSDate()
    let earliest = now.earlierDate(date as Date)
    let latest = (earliest == now as Date) ? date : now
    
    let unitsSet : Set<Calendar.Component> = [.year,.month,.weekOfYear,.day, .hour, .minute, .second, .nanosecond]
    
    let components:NSDateComponents =  calendar.dateComponents(unitsSet, from: earliest, to: latest as Date) as NSDateComponents
    
    if (components.year >= 2) {
        return "\(components.year) years ago"
    } else if (components.year >= 1){
        if (numericDates){
            return "1 year ago"
        } else {
            return "Last year"
        }
    } else if (components.month >= 2) {
        return "\(components.month) months ago"
    } else if (components.month >= 1){
        if (numericDates){
            return "1 month ago"
        } else {
            return "Last month"
        }
    } else if (components.weekOfYear >= 2) {
        return "\(components.weekOfYear) weeks ago"
    } else if (components.weekOfYear >= 1){
        if (numericDates){
            return "1 week ago"
        } else {
            return "Last week"
        }
    } else if (components.day >= 2) {
        return "\(components.day) days ago"
    } else if (components.day >= 1){
        if (numericDates){
            return "1 day ago"
        } else {
            return "Yesterday"
        }
    } else if (components.hour >= 2) {
        return "\(components.hour) hours ago"
    } else if (components.hour >= 1){
        if (numericDates){
            return "1 hour ago"
        } else {
            return "An hour ago"
        }
    } else if (components.minute >= 2) {
        return "\(components.minute) minutes ago"
    } else if (components.minute >= 1){
        if (numericDates){
            return "1 minute ago"
        } else {
            return "A minute ago"
        }
    } else if (components.second >= 3) {
        return "\(components.second) seconds ago"
    } else {
        return NSLocalizedString("justnow", tableName: nil, comment: "")
    }
}

func CompareDate (strFirstDate:String, strSecondDate:String) -> String
{
    var firstDateString :String = ""
    var SecondDateString :String = ""
    var strTime :String = ""
    let df = DateFormatter()
    df.dateFormat = "YYYY-MM-dd HH:mm:ss"
    firstDateString = strFirstDate
    SecondDateString = strSecondDate
    let date1: Date? = df.date(from: firstDateString)
    let date2: Date? = df.date(from: SecondDateString)
    //Calculating the time interval
    var secondsBetween1: TimeInterval? = nil
    
    if let aDate1 = date1
    {
        secondsBetween1 = date2?.timeIntervalSince(aDate1)
        let secondsBetween = Int(secondsBetween1!)
        let numberOfDays: Int = secondsBetween / 86400
        let timeResult = Int(secondsBetween) % 86400
        let hour: Int = timeResult / 3600
        let hourResult: Int = timeResult % 3600
        let minute: Int = hourResult / 60
        
        let secs: Int = minute % 3600
        let sec: Int = secs / 60
        
        if numberOfDays > 0 {
            if numberOfDays == 1 {
                strTime = String(numberOfDays) + " " + " day " + NSLocalizedString("ago", tableName: nil, comment: "")
            }
                
            else if numberOfDays > 7 && numberOfDays < 30{
                let week =  numberOfDays / 7
                if  week == 1{
                    strTime = String(week) + " " + " week " + NSLocalizedString("ago", tableName: nil, comment: "")
                }else{
                    strTime = String(week) + " " + " weeks " + NSLocalizedString("ago", tableName: nil, comment: "")
                }
                
            }else if numberOfDays > 30{
                let month =  numberOfDays / 30
                if  month == 1{
                    strTime = String(month) + " " + " month " + NSLocalizedString("ago", tableName: nil, comment: "")
                }else{
                    strTime = String(month) + " " + " months " + NSLocalizedString("ago", tableName: nil, comment: "")
                }
                
            } else if numberOfDays > 365{
                
                let year =  numberOfDays / 365
                if  year == 1{
                    strTime = String(year) + " " + " year " + NSLocalizedString("ago", tableName: nil, comment: "")
                }else{
                    strTime = String(year) + " " + " years " + NSLocalizedString("ago", tableName: nil, comment: "")
                }
                
            }else{
                strTime = String(numberOfDays) + " " + " days " + NSLocalizedString("ago", tableName: nil, comment: "")
            }
        }
        else if(numberOfDays == 0 && hour > 0)
        {
            if(numberOfDays == 0 && hour == 1)
            {
                strTime = String(hour) + " " + " hr " + NSLocalizedString("ago", tableName: nil, comment: "")
            }
            else
            {
                strTime = String(hour) + " " + " hrs " + NSLocalizedString("ago", tableName: nil, comment: "")
            }
        }
        else  if numberOfDays == 0 && hour == 0 && minute > 0 {
            if numberOfDays == 0 && hour == 0 && minute == 1 {
                
                strTime = String(minute) + " " +   " min " + NSLocalizedString("ago", tableName: nil, comment: "")
            }
            else
            {
                
                strTime = String(minute) + " " +  " mins " + NSLocalizedString("ago", tableName: nil, comment: "")
            }
        }
        else if numberOfDays == 0 && hour == 0 && minute == 0 && sec > 0
        {
            if numberOfDays == 0 && hour == 0 && minute == 0 && sec == 1{
                strTime = String(sec) + " " +  " sec " + NSLocalizedString("ago", tableName: nil, comment: "")
            }else{
                
                strTime = String(sec) + " " + " secs " + NSLocalizedString("ago", tableName: nil, comment: "")
            }
            
        }else {
            strTime = NSLocalizedString("justnow", tableName: nil, comment: "")
        }
    }
    return strTime
}

func relativePast(for date : Date) -> String {
    let units = Set<Calendar.Component>([.year, .month, .day, .weekOfYear])
    let components = Calendar.current.dateComponents(units, from: date, to: Date())
    
    let formatter = DateFormatter()
    formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
    let strDateTime = formatter.string(from: date)
    let arrDate = strDateTime.components(separatedBy: " ")
    
    var msgDateDay = 0
    var strDate = ""
    if arrDate.count == 2{
        let date1 = arrDate[0]
        var arrNew = date1.components(separatedBy: "-")
        arrNew = arrNew.reversed()
        msgDateDay = Int(arrNew[0])!
        strDate = arrNew.joined(separator: "/")
    }
    
    let currentDate = Date()
    let strCurrentDateTime = formatter.string(from: currentDate)
    let arrCurrentDate = strCurrentDateTime.components(separatedBy: " ")
    var currentDateDay = 0
    if arrCurrentDate.count == 2{
        let date2 = arrCurrentDate[0]
        var arrCurrentNew = date2.components(separatedBy: "-")
        arrCurrentNew = arrCurrentNew.reversed()
        currentDateDay = Int(arrCurrentNew[0])!
    }
    
    if components.year! > 0 {
        let str = converteDateMsgDay(strDateFromServer: strDate)
        return str
    } else if components.month! > 0 {
        let str = converteDateMsgDay(strDateFromServer: strDate)
        return str
    } else if components.weekOfYear! > 0 {
        let str = converteDateMsgDay(strDateFromServer: strDate)
        return str
    } else if (components.day! > 0) {
        if (components.day! > 1){
            let str = converteDateMsgDay(strDateFromServer: strDate)
            return str
        }else{
            let str = converteDateMsgDay(strDateFromServer: strDate)
            //return str
            return NSLocalizedString("yesterday", tableName: nil, comment: "")
        }
    } else {
        if currentDateDay>msgDateDay{
            return NSLocalizedString("yesterday", tableName: nil, comment: "")
        }else{
            return NSLocalizedString("today", tableName: nil, comment: "")
        }
    }
}

// Convert date formate
func converteDateMsgDay(strDateFromServer: String) -> (String) {
    
    var strConvertedDate : String = ""
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    
    let dateFromServer: Date? = dateFormatter.date(from: strDateFromServer)
    
    if let dateFromServer = dateFromServer {
        
        dateFormatter.dateFormat = "EEE, dd MMM"
        
        let strDate:String? = dateFormatter.string(from: dateFromServer)
        
        if let strDate = strDate {
            strConvertedDate = strDate
        }
    }
    return strConvertedDate
}

extension TimeInterval {
  var formatted: String {
    let formatter = DateComponentsFormatter()
    formatter.unitsStyle = .full
    formatter.allowedUnits = [.hour, .minute]

    return formatter.string(from: self) ?? ""
  }
}

