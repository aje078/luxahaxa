//
//  userDeafultsExt.swift
//  Elite
//
//  Created by Mac on 08/11/17.
//  Copyright © 2017 Mindiii. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults{

    enum Keys {
        
        static let updated_at = "updated_at"
        static let name = "name"
        static let id = "id"
        static let token = "token"
        static let email = "email"
        static let created_at = "created_at"
        static let mobile = "mobile"
        
        static let aadhar_file = "aadhar_file"
        static let aadhar_number = "aadhar_number"
        static let pan_file = "pan_file"
        static let email_verified_at = "email_verified_at"
        static let google_id = "google_id"
        static let bank_account_number = "bank_account_number"
        static let bank_name = "bank_name"
        static let facebook_id = "facebook_id"
        static let pan_number = "pan_number"
        static let profile_file = "profile_file"
        static let bank_branch_name = "bank_branch_name"
        static let phone = "phone"
        static let bank_ifsc_code = "bank_ifsc_code"
        
        static let month_buyed = "month_buyed"
        static let month_spent = "month_spent"
        static let user_balance = "user_balance"
        
        static let arrSelectedColors = "arrSelectedColors"
    }
}

