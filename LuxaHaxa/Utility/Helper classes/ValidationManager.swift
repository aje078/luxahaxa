//
//  ValidationManager.swift
//  UhungryApp
//
//  Created by Mindiii on 25/09/17.
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit

class ValidationManager: UIViewController {

    static let sharedManager = ValidationManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(strEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: strEmail)
        return result
    }
    
    func isNameString(nameStr:String)-> Bool{
        let nameRegEx = "^([A-Za-z](\\.)?+(\\s)?[A-Za-z|\\'|\\.]*){1,7}$"
        let nameTest = NSPredicate (format:"SELF MATCHES %@",nameRegEx)
        let result = nameTest.evaluate(with: nameStr)
        return result
    }
    
    func isNameLenth(name: String) -> Bool {
        if name.count <= 25 {
            return true
        }else{
            return false
        }
    }
    
    func isMinNameLenth(name: String) -> Bool {
        if name.count >= 3 {
            return true
        }else{
            return false
        }
    }
    
    func isMinCardNumber(Length: String) -> Bool {
        if Length.count == 19 {
            return true
        }else{
            return false
        }
    }
    
    func isMinCvvNumber(Length: String) -> Bool {
        if Length.count >= 3 {
            return true
        }else{
            return false
        }
    }
    
    func isPwd(value: String) -> Bool {
        if value.count > 6{
            return true
        }
        else{
            return false
        }
    }
    
    func isMinPhone(phone: String) -> Bool {
        
        if phone.count >= 6 {
            return true
        }else{
            return false
        }
    }
    func isMaxPhone(phone: String) -> Bool {
        
        if phone.count <= 16 {
            return true
        }else{
            return false
        }
    }
    func isPwdLenth(password: String) -> Bool {
        if password.count >= 6 {
            return true
        }else{
            return false
        }
    }
    
    func isConfPwdLenth(confirmPassword : String) -> Bool {
        if confirmPassword.count >= 6{
            return true
        }else{
            return false
        }
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: textField.center.x - 5, y: textField.center.y))
        
        animation.toValue = NSValue(cgPoint: CGPoint(x: textField.center.x + 5, y: textField.center.y))
        
        textField.layer.add(animation, forKey: "position")
        
    }
    
    func AnimationShakeTextView(textView:UITextView){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: textView.center.x - 5, y: textView.center.y))
        
        animation.toValue = NSValue(cgPoint: CGPoint(x: textView.center.x + 5, y: textView.center.y))
        
        textView.layer.add(animation, forKey: "position")
        
    }
    
    func AnimationShakeLabel(label:UILabel){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: label.center.x - 5, y: label.center.y))
        
        animation.toValue = NSValue(cgPoint: CGPoint(x: label.center.x + 5, y: label.center.y))
        
        label.layer.add(animation, forKey: "position")
        
    }
    
    //MARK:- Alert for validation
    func showAlert(message: String, title: String = "", controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", tableName: nil, comment: ""), style: .default, handler: nil)
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithAction(title:String,message:String,controller:UIViewController, completion: @escaping (_ status: Bool) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let subView = alertController.view.subviews.first!
        let alertContentView = subView.subviews.first!
        alertContentView.backgroundColor = UIColor.gray
        alertContentView.layer.cornerRadius = 20
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            completion(true)
        })
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK:- Enimation of view

    
    func showMainViewNN(viewMain: UIView, viewInside:UIView) {
    //        viewLang.frame = self.view.frame

    //        self.view.window?.addSubview(viewLang)
    //        viewLang.border1(UIColor.lightGray, 8, 0)
            viewMain.isHidden = false
            self.showPopUpAnimation(viewTest: viewInside)
        }
        
        
        func showPopUpAnimation(viewTest: UIView) {
            let orignalT: CGAffineTransform = viewTest.transform
            viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
            UIView.animate(withDuration: 0.3, animations: {
                
                viewTest.transform = orignalT
            }, completion:nil)
        }
        
        func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
            let orignalT: CGAffineTransform = viewPopUP.transform
            UIView.animate(withDuration: 0.3, animations: {
                viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            }, completion: {(sucess) in
    //            viewMainView.removeFromSuperview()
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                viewMain.isHidden = true
                viewPopUP.transform = orignalT

            }
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

//MARK:- validation for valid integer numbers
extension String {
    func isValidDouble(maxDecimalPlaces: Int) -> Bool {
     
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        let decimalSeparator = formatter.decimalSeparator ?? "."
        if formatter.number(from: self) != nil {
            let split = self.components(separatedBy: decimalSeparator)
            let digits = split.count == 2 ? split.last ?? "" : ""
            return digits.count <= maxDecimalPlaces
        }
        return false 
    }
}


