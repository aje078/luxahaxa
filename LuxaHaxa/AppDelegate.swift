//
//  AppDelegate.swift
//  LuxaHaxa
//
//  Created by Vijay on 04/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?
    var navigationcontroller: UINavigationController?
    
    static let sharedManager = AppDelegate()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

//        FirebaseApp.configure()
        self.navigateToLaunch()

//        GMSServices.provideAPIKey(kGoogleApiKey)
//        GMSPlacesClient.provideAPIKey(kGoogleApiKey)
        
        
        GIDSignIn.sharedInstance().clientID = "853133411387-dcli7quf9ehn0m7oqq1r3n4lo2kppesf.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
                
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func navigateToHome(){
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navHome") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        if #available(iOS 13.0, *) {
            UIApplication.shared.windows.first?.rootViewController = navigationcontroller
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            appDelegate.window?.rootViewController = navigationcontroller
            window?.makeKeyAndVisible()
        }
        
    }
    
    func navigateToLaunch(){
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navLaunch") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        if #available(iOS 13.0, *) {
            UIApplication.shared.windows.first?.rootViewController = navigationcontroller
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            appDelegate.window?.rootViewController = navigationcontroller
            window?.makeKeyAndVisible()
        }
        
    }
    
    func navigateToLogin(){
                
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navLogin") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if #available(iOS 13.0, *) {
            UIApplication.shared.windows.first?.rootViewController = navigationcontroller
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            appDelegate.window?.rootViewController = navigationcontroller
            window?.makeKeyAndVisible()
        }
    }
    
    func navigateToStartGame(){
                
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navStartGame") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if #available(iOS 13.0, *) {
            UIApplication.shared.windows.first?.rootViewController = navigationcontroller
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            appDelegate.window?.rootViewController = navigationcontroller
            window?.makeKeyAndVisible()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            
            
            ApplicationDelegate.shared.application(
                app,
                open: url,
                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplication.OpenURLOptionsKey.annotation]
            )

    //        return GIDSignIn.sharedInstance().handle(url)
            
            
            let googleDidHandle = GIDSignIn.sharedInstance().handle(url)

            let facebookDidHandle = ApplicationDelegate.shared.application(
                app, open: url, options: options)
            
            

            return googleDidHandle || facebookDidHandle
            
            
        }
        
}


