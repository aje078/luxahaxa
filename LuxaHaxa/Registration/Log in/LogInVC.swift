//
//  LogInVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 04/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class LogInVC: BaseViewController, UITextFieldDelegate, GIDSignInDelegate {
    
    var strEmail = ""
    var strFullName = ""
    var strProvider = ""
    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var viewInUserName: UIView!
    @IBOutlet weak var viewInPassword: UIView!
    @IBOutlet weak var hViewInUserName: NSLayoutConstraint!
    @IBOutlet weak var hViewInPassword: NSLayoutConstraint!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    
    @IBOutlet weak var bLblUserName: NSLayoutConstraint!
    @IBOutlet weak var bLblPassword: NSLayoutConstraint!
    
    @IBOutlet weak var viewFacebook: UIView!
    @IBOutlet weak var viewGoogle: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViewController()
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        // GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        GIDSignIn.sharedInstance()?.delegate = self
       
    }
        
    func navigateWithSocialLogin(isFacebook:Bool, socialId:String){
        
        let dict = ["name": self.strFullName, "email": self.strEmail, "mobile": "1234567890", "password": "123456", "password_confirmation": "123456", "socialLoginId": socialId]

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionVC") as! Terms_ConditionVC
        vc.dictSocial = dict
        vc.isFacebookLogin = isFacebook
        vc.isFromSocial = true
                
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    // MARK: - Webservice Methods
    func callWebServie_Customer_Login(){
       
        let dict = ["email": self.txtUserName.text!, "password": self.txtPassword.text!]
        
        WebServiceManager.sharedManager.requestPost(strURL: kUrlSignIn, params: dict, success: { (response) in
            print(response)
            
            let error = response["message"] as? String
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let token = data["token"] as? String ?? ""
            if code == 200 || status == "success"{
                
                let _ = UserDataModal.init(dict: user, token: token)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                ValidationManager.sharedManager.showAlert(message: error ?? "Something went wrong, please try agian", title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try again", title: "Alert", controller: self)
            
        }
    }
    
    func callWebServie_Customer_SocialLogin(isFacebook:Bool, dict:[String:Any]){
        
        let name = dict["name"] as? String ?? ""
        let email = dict["email"] as? String ?? ""
        let mobile = dict["mobile"] as? String ?? ""
        let password = dict["password"] as? String ?? ""
        let confirmPassword = dict["password_confirmation"] as? String ?? ""
        let socialLoginId = dict["socialLoginId"] as? String ?? ""
        
        var urlType = kUrlGoogleSignIn
        var socialKey = "google_id"
        if isFacebook == true{
            urlType = kUrlFacebookSignIn
            socialKey = "facebook_id"
        }
       
        let dict = ["name": name, "email": email, socialKey: socialLoginId]
        
        WebServiceManager.sharedManager.requestPost(strURL: urlType, params: dict, success: { (response) in
            print(response)
            
            let error = response["error"] as? [String]
            var errMsg = "Something went wrong, please try agian"
            if error?.count ?? 0 > 0{
                errMsg = error?[0] ?? "Something went wrong, please try agian"
            }
            
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let token = data["token"] as? String ?? ""
            if code == 200 || status == "success"{
                
                let _ = UserDataModal.init(dict: user, token: token)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                ValidationManager.sharedManager.showAlert(message: errMsg, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try agian", title: "Alert", controller: self)
            
        }
    }
    
    // MARK: - UIButton Methods
    @IBAction func btnLogIn(_ sender: Any) {
        self.view.endEditing(true)
        
        if (txtUserName.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter Username", title: "Alert", controller: self)
        }else if (txtPassword.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter password", title: "Alert", controller: self)
        }else if !ValidationManager.sharedManager.isPwdLenth(password: txtPassword.text!) {
            ValidationManager.sharedManager.showAlert(message: "Password should be atleast 6 characters long", title: "Alert", controller: self)
        }else{
            
            self.callWebServie_Customer_Login()
        }
    }
    @IBAction func btnSignUp(_ sender: Any) {
        self.view.endEditing(true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionVC") as! Terms_ConditionVC
        vc.isFromSocial = false
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        self.view.endEditing(true)
        //        self.navigate_mobileGoogle()
        
        GIDSignIn.sharedInstance().signIn()
        
        //        GIDSignIn.sharedInstance().signOut()
        
    }
    
    @IBAction func btnFaceBook(_ sender: Any) {
        self.view.endEditing(true)
        
                let fbLoginManager : LoginManager = LoginManager()
                fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
                  if (error == nil){
                    let fbloginresult : LoginManagerLoginResult = result!
                    // if user cancel the login
                    if (result?.isCancelled)!{
                            return
                    }
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                      self.getFBUserData()
                    }
                  }
                }
    }
    
    func configureViewController(){
        
        txtUserName.delegate = self
        txtPassword.delegate = self
        
        self.hViewInUserName.constant = 1
        self.hViewInPassword.constant = 1
        
        self.viewFacebook.setCornerRadius(cornerRadius: 20, borderColor: nil, borderWidth: nil)
        self.viewGoogle.setCornerRadius(cornerRadius: 20, borderColor: nil, borderWidth: nil)

    }
    
    // MARK: - Text Field Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUserName{
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword{
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtUserName{
            if self.txtUserName.text?.count == 0{
                self.bLblUserName.constant = 3
                self.lblUserName.font = lblUserName.font.withSize(17)
            }
        }else if textField == txtPassword{
            if self.txtPassword.text?.count == 0{
                self.bLblPassword.constant = 3
                self.lblPassword.font = lblPassword.font.withSize(17)
            }
        }
                
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtUserName{

            self.hViewInUserName.constant = 2
            self.hViewInPassword.constant = 1
            
            self.bLblUserName.constant = 20
            self.lblUserName.font = lblUserName.font.withSize(14)
            if self.txtPassword.text?.count == 0{
                self.bLblPassword.constant = 3
                self.lblPassword.font = lblPassword.font.withSize(17)
            }
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtPassword{

            self.hViewInUserName.constant = 1
            self.hViewInPassword.constant = 2

            self.bLblPassword.constant = 20
            self.lblPassword.font = lblUserName.font.withSize(14)
            if self.txtUserName.text?.count == 0{
                self.bLblUserName.constant = 3
                self.lblUserName.font = lblUserName.font.withSize(17)
            }
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }

        return true

    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField == txtUserName{
//
//            let currentText = txtUserName.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInUserName.constant = 2
//                self.bLblUserName.constant = 20
//                self.lblUserName.font = lblUserName.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//            }
//
//        }else if textField == txtPassword{
//
//            let currentText = txtPassword.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInPassword.constant = 2
//                self.bLblPassword.constant = 20
//                self.lblPassword.font = lblPassword.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//
//            }
//
//        }
//
//        return true
//    }

    func height_required(){
        
        self.hViewInUserName.constant = 1
        self.hViewInPassword.constant = 1
        
        self.lblUserName.text! = "USERNAME"
        self.lblPassword.text! = "PASSWORD"
        
        if self.txtUserName.text?.count == 0{
            self.bLblUserName.constant = 3
            self.lblUserName.font = lblUserName.font.withSize(17)
        }
        
        if self.txtPassword.text?.count == 0{
            self.bLblPassword.constant = 3
            self.lblPassword.font = lblPassword.font.withSize(17)
        }
    }

}

//MARK:- Google Login
extension LogInVC{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
          if let error = error {
          if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
            print("The user has not signed in before or they have since signed out.")
          } else {
            print("\(error.localizedDescription)")
          }
          return
        }
        // Perform any operations on signed in user here.
//        self.gUserId = user.userID                  // For client-side use only!
//        self.gIdToken = user.authentication.idToken // Safe to send to the server
//        self.gGivenName = user.profile.givenName
//        self.gFamilyName = user.profile.familyName
        
        let gFullName = user.profile.name
        let gEmail = user.profile.email
        let gEmailUserId = user.userID ?? ""
        
        self.strEmail = gEmail ?? ""
        self.strFullName = gFullName ?? ""
        self.strProvider = "Google"
        
        self.navigateWithSocialLogin(isFacebook:false, socialId: gEmailUserId)
        
        let dimension = round(100 * UIScreen.main.scale)
        if let pic = user.profile.imageURL(withDimension: UInt(dimension)){
            UserDefaults.standard.set(pic.absoluteString, forKey: "gProfileImage")
            print(pic.absoluteString)
            
//            self.gProfileImage = pic.absoluteString
            
        }
        
//        AppDelegate.sharedObjAppDelegate.navigateToHome()
    
    }
    
    
}

//MARK:- Facebook Login
extension LogInVC{
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result)
                    let response = result as? [String:Any]
                    
                    let fEmail = response?["email"] as? String ?? ""
                    
                    let fFirstName = response?["first_name"] as? String ?? ""
                    let fLastName = response?["last_name"] as? String ?? ""
                    let fUserId = response?["id"] as? String ?? ""
                    
                    let fullName = fFirstName+" "+fLastName
                    
                    self.strEmail = fEmail
                    self.strFullName = fullName
                    self.strProvider = "Facebook"
                  
                    self.navigateWithSocialLogin(isFacebook:true, socialId: fUserId)
                }
            })
        }
    }
}

    
