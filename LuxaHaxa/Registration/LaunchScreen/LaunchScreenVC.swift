//
//  LaunchScreenVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 07/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class LaunchScreenVC: UIViewController {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            
            let userId = UserDefaults.standard.string(forKey: UserDefaults.Keys.id) ?? ""
            let token = UserDefaults.standard.string(forKey: UserDefaults.Keys.token) ?? ""
            if userId != "" || token != ""{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: false)
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }

        self.imgIcon.rotateWithAnimation(angle: 360°)
        self.lbltitle.rotateWithAnimation(angle: -360°)
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.lbltitle.rotateWithAnimation(angle: 360°)
            self.imgIcon.rotateWithAnimation(angle: -360°)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        

    }

}

postfix operator °

protocol IntegerInitializable: ExpressibleByIntegerLiteral {
    init (_: Int)
}

extension Int: IntegerInitializable {
    postfix public static func °(lhs: Int) -> CGFloat {
        return CGFloat(lhs) * .pi / 180
    }
}

extension CGFloat: IntegerInitializable {
    postfix public static func °(lhs: CGFloat) -> CGFloat {
        return lhs * .pi / 180
    }
}

extension UIView {
    func rotateWithAnimation(angle: CGFloat, duration: CGFloat? = nil) {
        let pathAnimation = CABasicAnimation(keyPath: "transform.rotation")
        pathAnimation.duration = CFTimeInterval(duration ?? 0.5)
        pathAnimation.fromValue = 0
        pathAnimation.toValue = angle
        pathAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        self.transform = transform.rotated(by: angle)
        self.layer.add(pathAnimation, forKey: "transform.rotation")
    }
}
