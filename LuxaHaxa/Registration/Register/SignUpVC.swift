//
//  SignUpVC.swift
//  LuxaHaxa
//
//  Created by Vijay on 04/11/20.
//  Copyright © 2020 Drive2uApp. All rights reserved.
//

import UIKit

class SignUpVC: BaseViewController, UITextFieldDelegate {

    var strCheck:Bool = false

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var viewInUserName: UIView!
    @IBOutlet weak var viewInEmail: UIView!
    @IBOutlet weak var viewInMobile: UIView!
    @IBOutlet weak var viewInPassword: UIView!
    @IBOutlet weak var viewInConfirmPassword: UIView!
    
    @IBOutlet weak var imgCheck: UIImageView!
    
    @IBOutlet weak var hViewInUserName: NSLayoutConstraint!
    @IBOutlet weak var hViewInEmail: NSLayoutConstraint!
    @IBOutlet weak var hViewInMobile: NSLayoutConstraint!
    @IBOutlet weak var hViewInPassword: NSLayoutConstraint!
    @IBOutlet weak var hViewInConfirmPassword: NSLayoutConstraint!

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEMail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblConfrimPassword: UILabel!

    @IBOutlet weak var bLblUserName: NSLayoutConstraint!
    @IBOutlet weak var bLblEmail: NSLayoutConstraint!
    @IBOutlet weak var bLblMobile: NSLayoutConstraint!
    @IBOutlet weak var bLblPassword: NSLayoutConstraint!
    @IBOutlet weak var bLblConfirmPassword: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViewController()
       
    }
    
    func configureViewController(){
        self.height_required()
        self.imgCheck.setCornerRadius(cornerRadius: 0.5, borderColor: UIColor.white, borderWidth: 2)

        txtUserName.delegate = self
        txtEmail.delegate = self
        txtMobile.delegate = self
        txtPassword.delegate = self
        txtConfirmPassword.delegate = self

    }
    
    // MARK: - Webservice Methods
    func callWebServie_Customer_Register(){
       
        let dict = ["name": self.txtUserName.text!, "email": self.txtEmail.text!, "mobile": self.txtMobile.text!, "password": self.txtPassword.text!, "password_confirmation": self.txtConfirmPassword.text!]
        
        WebServiceManager.sharedManager.requestPost(strURL: kUrlSignUp, params: dict, success: { (response) in
            print(response)
            
            let error = response["error"] as? [String]
            var errMsg = "Something went wrong, please try agian"
            if error?.count ?? 0 > 0{
                errMsg = error?[0] as? String ?? "Something went wrong, please try agian"
            }
            
            let code = response["code"] as? Int
            let status = response["status"] as? String ?? ""
            let data = response["data"] as? [String:Any] ?? [:]
            let user = data["user"] as? [String:Any] ?? [:]
            let token = data["token"] as? String ?? ""
            if code == 200 || status == "success"{
                
                let _ = UserDataModal.init(dict: user, token: token)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                ValidationManager.sharedManager.showAlert(message: errMsg, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            ValidationManager.sharedManager.showAlert(message: "Something went wrong, please try agian", title: "Alert", controller: self)
            
        }
    }
    
    // MARK: - UIButton Methods
    @IBAction func btnCheck(_ sender: Any) {
        self.view.endEditing(true)
        if strCheck == false{
            imgCheck.image = UIImage(named: "check")
            self.strCheck = true
        }else{
            imgCheck.image = nil
            self.strCheck = false
        }
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        self.view.endEditing(true)
        
        let new = txtPassword.text!
        let confirm = txtConfirmPassword.text!

        if (txtUserName.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter Username", title: "Alert", controller: self)
        }else if (txtEmail.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter email", title: "Alert", controller: self)
        }else if !ValidationManager.sharedManager.isValidEmail(strEmail: txtEmail.text!){

            ValidationManager.sharedManager.showAlert(message: "Please enter valid email id", title: "Alert", controller: self)
        }else if (txtMobile.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter mobile number", title: "Alert", controller: self)
        }else if (txtPassword.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter valid password", title: "Alert", controller: self)
        }else if !ValidationManager.sharedManager.isPwdLenth(password: txtPassword.text!) {

            ValidationManager.sharedManager.showAlert(message: "Password should be atleast 6 characters long", title: "Alert", controller: self)
        }else if (txtConfirmPassword.text?.isEmpty)!
        {
            ValidationManager.sharedManager.showAlert(message: "Please enter valid confirm password", title: "Alert", controller: self)
        }else if !ValidationManager.sharedManager.isPwdLenth(password: txtConfirmPassword.text!) {

            ValidationManager.sharedManager.showAlert(message: "Confirm password should be atleast 6 characters long", title: "Alert", controller: self)
        }else if new != confirm
        {
            ValidationManager.sharedManager.showAlert(message: "Password and confirm password not matched, please try again", title: "Alert", controller: self)
        }else if self.imgCheck.image == nil
        {
            ValidationManager.sharedManager.showAlert(message: "Please agree terms and conditions", title: "Alert", controller: self)
        }else{
            
            self.callWebServie_Customer_Register()
        }
    }
    
    // MARK: - Text Field Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUserName{
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail{
            txtMobile.becomeFirstResponder()
        }else if textField == txtMobile{
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword{
            txtConfirmPassword.becomeFirstResponder()
        }else if textField == txtConfirmPassword{
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField == txtUserName{
//
//
//
//            let currentText = txtUserName.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInUserName.constant = 2
//                self.bLblUserName.constant = 20
//                self.lblUserName.font = lblUserName.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//            }
//
//
//        }else if textField == txtEmail{
//            self.height_required()
//            self.hViewInEmail.constant = 2
//            let currentText = txtEmail.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//
//                self.bLblEmail.constant = 20
//                self.lblEMail.font = lblEMail.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//
//            }
//
//        }else if textField == txtMobile{
//
//            let currentText = txtMobile.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInMobile.constant = 2
//                self.bLblMobile.constant = 20
//                self.lblMobile.font = lblMobile.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//
//            }
//
//        }else if textField == txtPassword{
//
//            let currentText = txtPassword.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInPassword.constant = 2
//                self.bLblPassword.constant = 20
//                self.lblPassword.font = lblMobile.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//
//            }
//
//        }else if textField == txtConfirmPassword{
//
//            let currentText = txtConfirmPassword.text ?? ""
//            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//
//            if replacementText.count > 0{
//                self.height_required()
//                self.hViewInConfirmPassword.constant = 1
//                self.bLblConfirmPassword.constant = 20
//                self.lblConfrimPassword.font = lblMobile.font.withSize(14)
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//
//            }else{
//
//
//            }
//
//        }
//
//
//        return true
//
//
//    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtUserName{

            self.height_required()
            self.hViewInUserName.constant = 2

            self.bLblUserName.constant = 20
            self.lblUserName.font = lblUserName.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtEmail{
            self.height_required()
            self.hViewInEmail.constant = 2

            self.bLblEmail.constant = 20
            self.lblEMail.font = lblEMail.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtMobile{
            self.height_required()
            self.hViewInMobile.constant = 2

            self.bLblMobile.constant = 20
            self.lblMobile.font = lblMobile.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtPassword{
            self.height_required()
            self.hViewInPassword.constant = 2

            self.bLblPassword.constant = 20
            self.lblPassword.font = lblMobile.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtConfirmPassword{
            self.height_required()
            self.hViewInConfirmPassword.constant = 1

            self.bLblConfirmPassword.constant = 20
            self.lblConfrimPassword.font = lblMobile.font.withSize(14)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }

        return true

    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtUserName{
            if self.txtUserName.text?.count == 0{
                self.bLblUserName.constant = 3
                self.lblUserName.font = lblUserName.font.withSize(17)
            }
        }else if textField == txtEmail{
            if self.txtEmail.text?.count == 0{
                self.bLblEmail.constant = 3
                self.lblEMail.font = lblEMail.font.withSize(17)
            }
        }else if textField == txtMobile{
            if self.txtMobile.text?.count == 0{
                self.bLblMobile.constant = 3
                self.lblMobile.font = lblMobile.font.withSize(17)
            }
        }else if textField == txtPassword{
            if self.txtPassword.text?.count == 0{
                self.bLblPassword.constant = 3
                self.lblPassword.font = lblPassword.font.withSize(17)
            }
        }else if textField == txtConfirmPassword{
            if self.txtConfirmPassword.text?.count == 0{
                self.bLblConfirmPassword.constant = 3
                self.lblConfrimPassword.font = lblConfrimPassword.font.withSize(17)
            }
        }
        
        return true
    }
    
    func height_required(){
        
        self.hViewInUserName.constant = 1
        self.hViewInEmail.constant = 1
        self.hViewInMobile.constant = 1
        self.hViewInPassword.constant = 1
        self.hViewInConfirmPassword.constant = 1
        
        self.lblUserName.text! = "USERNAME"
        self.lblEMail.text! = "EMAIL"
        self.lblMobile.text! = "MOBILE"
        self.lblPassword.text! = "PASSWORD"
        self.lblConfrimPassword.text! = "CONFIRMED PASSWORD"
        
        if self.txtUserName.text?.count == 0{
            self.bLblUserName.constant = 3
            self.lblUserName.font = lblUserName.font.withSize(17)
        }
        
        if self.txtEmail.text?.count == 0{
            self.bLblEmail.constant = 3
            self.lblEMail.font = lblEMail.font.withSize(17)
        }
        
        if self.txtMobile.text?.count == 0{
            self.bLblMobile.constant = 3
            self.lblMobile.font = lblMobile.font.withSize(17)
        }
        
        if self.txtPassword.text?.count == 0{
            self.bLblPassword.constant = 3
            self.lblPassword.font = lblPassword.font.withSize(17)
        }
        
        if self.txtConfirmPassword.text?.count == 0{
            self.bLblConfirmPassword.constant = 3
            self.lblConfrimPassword.font = lblConfrimPassword.font.withSize(17)
        }
    }

    
}
